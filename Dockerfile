FROM trion/ng-cli-e2e

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json
RUN npm install

# build app
COPY . /app
RUN npm run build:prod

# serve app
CMD npm start
