console.log("Try to set attribute username");
// set raw username
var rawusername;
var parentdoc;
try {
  parentdoc = parent.document;
} catch(e) {
  console.log("Cannot access parent document. Error: ", e);
}
if (parentdoc) {
  var opalcontent = parentdoc.querySelector('[title="Profil anzeigen"]');
  if (opalcontent) {
    rawusername = opalcontent.text;
  } else {
    var moodlesubselect = parentdoc.querySelector('[class="userbutton"]');
    if (moodlesubselect) {
      var moodlecontent = moodlesubselect.querySelector('[class~="usertext"]').innerText;
      if (moodlecontent) {
        rawusername = moodlecontent;
      }
    }
  }
  if (rawusername) {
    var username = rawusername.toString().replace('\t', '').trim();
    var wb = document.getElementById("wbopal1");
    wb.setAttribute("username", username);
  }
}