export const environment = {
  production: true,
  prefixRedirectUri: "https://workbench.tech4comp.dbis.rwth-aachen.de/#",
  //prefixRedirectUri: "http://localhost:4200",
  //wbServiceUri: "http://localhost:8080/mwb",
  wbServiceUri: "https://workbench.tech4comp.dbis.rwth-aachen.de/api/mwb",

  version: require("../../package.json").version
};
