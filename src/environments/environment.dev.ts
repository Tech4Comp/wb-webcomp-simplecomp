export const environment = {
  production: true,
  prefixRedirectUri: "http://localhost:4200",
  wbServiceUri: "http://localhost:8080/mwb",
  version: require("../../package.json").version
};
