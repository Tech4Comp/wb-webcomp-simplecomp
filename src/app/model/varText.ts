import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

// handle variable texts
// depending on type ('text', 'url', 'file') value will contain a string (either text or an url) or the id for an uploaded file

export class VarText {
  type: string = 'text';   
  value: string | number = '';
}
