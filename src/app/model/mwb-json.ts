import { Injectable } from '@angular/core';
import { ExamFeedback } from './examFeedback';
import { WritingTask } from './textAnalysis';

@Injectable({
  providedIn: 'root'
})


export class MwbJson {
  version:string;
  resources: JsonResource[] = [];
  interventions: JsonIntervention[] = [];
  examFeedbacks: ExamFeedback[] = [];
  textAnalysis: WritingTask[] = [];
}

export class JsonResource {
  id = '';
  resourceIntro = '';
  interventionIds: string[] = [];
  searchId = '';
  privacyStatement = '';
  disagreeStatement = '';
  confirmStatement = '';
  removeSuccessStatement = '';
  resourceDetails = '';
  lastChanged = 0;
  onlyOneIntervention = false;
}

export class JsonIntervention {
  id = '';
  name = '';
  type = '';
  searchId = '';
  botnameFW = '';
  botnameRocketChat = '';
  boturl = '';
  intro = '';
  description = '';
  detailsIntro = '';
  content = '';
  contentBefore = '';
  hasToDo = false;   //weiß nicht, ob das bleibt
  tasks = 0;
  fulfilledTasks = 0;
  eraseWarning = '';
  interventionHints = '';   //kann vlt. bald weg?
  interventionHelp = '';
  interventionFeedback = '';
  date = 0;   //when the exam takes place
  examName = '';
  lastChanged = 0;
}


