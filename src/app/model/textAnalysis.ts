import { Injectable } from '@angular/core';
import { VarText } from './varText';

@Injectable({
  providedIn: 'root'
})

export class TextAnalysis {
  interventionId = '';
  lrsId = '';
  eraseWarning = '';
}

export class WritingTask {
  id = '';
  name = '';
  interventionId = '';
  visible = 0;
  visibleFrom = 0;
  visibleTo = 0;
  task: VarText;
  compareToText: ''; // uploaded file 
  enablePeerComparison = false;
  lastChanged = 0;
}