import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class IntentDistribution{
  name: string;
  id: string;
  value: number;
}
