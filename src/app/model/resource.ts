import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Resource {
  id = '';
  name = '';
  description = '';
  searchId = '';
  privacyStatement = '';
  disagreeStatement = '';
  confirmStatement = '';
  removeSuccessStatement = '';
  lastChanged = 0;
}
