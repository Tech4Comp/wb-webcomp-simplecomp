import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class IntentPath{
  nodes: PathNode[];
  edges: PathEdge[];
}

export class PathNode{
  label: string;
  type: string;
  value: number;
  id: number;
  color?: any;
}

export class PathEdge{
  from: number;
  to: number;
  title: string;
  value: number;
  color?: any;
  dashes?: any;
}
