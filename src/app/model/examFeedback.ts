import { Injectable } from '@angular/core';
import { VarText } from './varText';

@Injectable({
  providedIn: 'root'
})


export class ExamFeedbacks {
  interventionId = '';
  lrsId = '';
}

export class ExamFeedback {
  id = '';
  name = '';
  interventionId = '';
  visible = 0;
  visibleFrom = 0;
  visibleTo = 0;
  contentBeforeExam = '';
  contentAfterExam = '';
  infoText: VarText;
  easlitProjectId = '';
  onyxTestId = '';
  feedbackTopic = '';
  lastChanged = 0;
}