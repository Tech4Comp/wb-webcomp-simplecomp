import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class BotInfo {
  id = '';
  nameFramework = '';
  authToken = '';
}
