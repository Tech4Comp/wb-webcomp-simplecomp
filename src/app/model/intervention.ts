import { Injectable } from '@angular/core';
import { ExamFeedback } from './examFeedback';
import { WritingTask } from './textAnalysis';

@Injectable({
  providedIn: 'root'
})
export class Intervention {
  id = '';
  name = '';
  resourceId = '';
  type = '';
  searchId = false;
  description = '';
  introText = '';
  hints = '';
  helpText = '';
  feedbackText = '';
  lastChanged = 0;
}

export enum InterventionType {
  None='',
  TextAnalysis='textAnalysis', // 'schreibanlass',
  ExamFeedback='examFeedback', // 'testFeedback',
  SimpleBot='simpleBot',
  KnowledgeGraph='knowledgeGraph',
  TextAnalysisWithKnowledgeGraph='textAnalysisWithKnowledgeGraph'
}

export function getInterventionTypes() : string[] {
  let types :string[] = [];
  for (let intervention in InterventionType) {
    types.push(intervention);
  }
  return types;
}

function isValidInterventionType(type : string) : boolean {
  if (type === InterventionType.None)
    return false;
  return true;
}

export function getValidInterventionTypes() : string[] {
  return getInterventionTypes().filter(isValidInterventionType);
}

export function getEnumInterventionType(type: string) : string {
  for (let interventionType in InterventionType) {
    if (type == InterventionType[interventionType])
      return interventionType;
  }
  return type;
}

export function getShortTextForInterventionType(type: string, isEnumKey: boolean = false) {
  type = isEnumKey ? InterventionType[type] : type;
  switch (type) {
    case InterventionType.ExamFeedback:
      return 'Feedback für Prüfungen';
    case InterventionType.TextAnalysis:
      return 'Textanalyse';
    case InterventionType.SimpleBot:
      return 'Chatbot';
    case InterventionType.KnowledgeGraph:
      return 'Wissenslandkarte';
  }
  return isEnumKey ? type : getEnumInterventionType(type);
}

export function getLongTextForInterventionType(type: string, isEnumKey: boolean = false) {
  type = isEnumKey ? InterventionType[type] : type;
  switch (type) {
    case InterventionType.ExamFeedback:
      return 'Erweitertes Feedback für Prüfungen mit EAS.lit';
    }
  return getShortTextForInterventionType(type);
}



export function GetInterventionItemsName(type : string, single: boolean = true) : string {
  switch (type) {
    case InterventionType.ExamFeedback:
      return single ? 'Feedback zu Test' : 'Feedback zu Tests';
    case InterventionType.TextAnalysis:
      return single ? 'Schreibaufgabe' : 'Schreibaufgaben';
    }
  return '';
}

export function IsInterventionWithDetails(type : string) : boolean {
  return GetInterventionItemsName(type) != '';
}

export function IsInterventionWithBot(type : string) : boolean {
  switch (type) {
    case InterventionType.TextAnalysis:
    case InterventionType.TextAnalysisWithKnowledgeGraph:
    case InterventionType.SimpleBot:
      return true;
  }
  return false;
}

export function IsInterventionWithTextAnalysis(type : string) : boolean {
  switch (type) {
    case InterventionType.TextAnalysis:
    case InterventionType.TextAnalysisWithKnowledgeGraph:
      return true;
  }
  return false;
}

export function isVisibleInterventionItem(item: WritingTask | ExamFeedback) {
  if (!item.visible)
    return false;
  if (item.visible == 1)
    return true;
  let now = Date.now();
  if (item.visibleFrom > now)
    return false;
  if (item.visibleTo < now)
    return false;
  return true;
}
