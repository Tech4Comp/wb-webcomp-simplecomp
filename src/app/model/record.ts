import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class Record { 
  filename: string;
  statementId: string;
  stored: string;
  storedDisplay: string;
  fileInfo: RecordFileInfo;
}

export class RecordFileInfo {
  assignmentNumber: string;
  analysisType: string;
  name1: string;
  name2: string;
}

export class SmallRecord {   //only what's necessary for the knowledge graph tool
  statementID: string;
  filename: string;
  time: string;
}

export function extractSmallRecord(record: Record) : SmallRecord {
  let smallRecord = new SmallRecord;
  smallRecord.statementID = record.statementId;
  smallRecord.filename = record.filename;
  smallRecord.time = record.stored;
  return smallRecord;
}
 