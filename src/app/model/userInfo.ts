import { Injectable } from '@angular/core';
import { UserInfo as UserProfile } from 'angular-oauth2-oidc';
import { MwbConfiguration } from '../configuration/mwb.config';

@Injectable({ providedIn: 'root' })
export class UserInfo {
  public resourceId: string = '';
  public interventionId: string = '';
  public title: string = '';
  private userProfile: UserProfile = null;

  public get name() {
    return this.userProfile ? this.userProfile.info['name'] : '';
  }

  constructor(
    private mwb: MwbConfiguration
  ) {}

  public isValid() : boolean {
    return this.userProfile != null;
  }

  public setTitle(title: string){
    this.title = title;
  }

  public setUserProfile(userProfile: object) {
    this.userProfile = userProfile as UserProfile;
  }

  public setResourceId(resId: string){
    this.resourceId = resId;
  }

  public setInterventionId(intId: string) {
    this.interventionId = intId;
    if (intId) {
      this.resourceId = this.mwb.configuration.getResourceIdForIntervention(intId);
    }
  }

  public getProfileData(id: string) {
    return this.userProfile ? this.userProfile.info[id] : '';
  }

  public unset() {
    this.userProfile = null;
    this.setResourceId('');
    this.setInterventionId('');
  }
}
