import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})


export class SearchLearningResources {
  search_id = '';
  index_name = '';
  title = '';
  examples = '';
  data_url = '';
  video_url = '';
  video_slides_map = '';
  main_category = [];
  search_category = [];
  lastChanged = 0;
}