import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { OAuthModule } from 'angular-oauth2-oidc';
import { TrustHtml } from './common/trust.html.pipe';
import { Router } from '@angular/router';
import { HashLocationStrategy, Location, LocationStrategy } from "@angular/common";
import { MwbConfigurationFromJson } from './configuration/mwb-json.config';
import { MwbConfiguration } from './configuration/mwb.config';
import { NgMentoringWorkbenchComponent } from './ng-mentoring-workbench/ng-mentoring-workbench.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { MentorViewNavigationComponent } from './view/mentor/mentor-view-navigation/mentor-view-navigation.component';
import { ResourceComponent } from './view/resource/resource/resource.component';
import { ResourceEditComponent } from './view/mentor/resource-edit/resource-edit.component';
import { ResourceEditInterventionsComponent } from './view/mentor/resource-edit-interventions/resource-edit-interventions.component';
import { ResourceMentorResultsComponent } from './view/mentor/resource-mentor-results/resource-mentor-results.component';
import { ExamFeedbackComponent } from './view/intervention/examFeedback/examFeedback.component';
import { TextAnalysisComponent } from './view/intervention/textAnalysis/textAnalysis.component';
import { InterventionComponent } from './view/intervention/intervention.component';
import { SimpleBotComponent } from './view/intervention/simple-bot/simple-bot.component';
import { KnowledgeGraphComponent } from './view/intervention/knowledge-graph/knowledge-graph.component';
import { OverviewComponent } from './view/resource/overview/overview.component';
import { LoginComponent } from './view/basic/login/login.component';
import { LoginService } from './services/login-service';
import { PrivacyComponent } from './view/basic/privacy/privacy.component';
import { ResourceInfoComponent } from './view/resource/resource-info/resource-info.component';
import { WelcomeComponent } from './view/basic/welcome/welcome.component';
import { UserInfo } from './model/userInfo';
import { LrsAnalyticsComponent } from './view/lrs-analytics/lrs-analytics.component';
import { BotAnalysisComponent } from './view/bot-analysis/bot-analysis.component';
import { RocketChatComponent } from './view/intervention/rocket-chat/rocket-chat.component';
import { LogoutComponent } from './view/basic/logout/logout.component';
import { ChartsModule } from 'ng2-charts';
import { InterventionEditComponent } from './view/mentor/intervention-edit/intervention-edit.component';
import { HelpComponent } from './view/utils/help/help.component';
import { HelpConfigurationFromJson } from './configuration/help.config';
import { ExamFeedbackEditComponent } from './view/mentor/exam-feedback-edit/exam-feedback-edit.component';
import { GetPeriodComponent } from './view/utils/get-period/get-period.component';
import { GetVisibilityComponent } from './view/utils/get-visibility/get-visibility.component';
import { WritingTaskEditComponent } from './view/mentor/writing-task-edit/writing-task-edit.component';
import { SimpleBotEditComponent } from './view/mentor/simple-bot-edit/simple-bot-edit.component';
import { UploadFileComponent } from './view/utils/upload-file/upload-file.component';
import { GetVarTextComponent } from './view/utils/get-var-text/get-var-text.component';
import { CircularProgressBarComponent } from './view/utils/circular-progress-bar/circular-progress-bar.component';
import { BotInfoTextsComponent } from './view/intervention/bot-info-texts/bot-info-texts.component';
import { TextAnalysisHistoryComponent } from './view/intervention/text-analysis-history/text-analysis-history.component';
import { TextAnalysisKnowledgeGraphComponent } from './view/intervention/text-analysis-knowledge-graph/text-analysis-knowledge-graph.component';
import { SearchComponent } from './view/search/search.component';
import { PopupSearchResultComponent } from './view/utils/popup-search-result/popup-search-result.component';

@NgModule({
  declarations: [
    NgMentoringWorkbenchComponent,
    TrustHtml,
    ResourceInfoComponent,
    ResourceComponent,
    ResourceEditComponent,
    ExamFeedbackComponent,
    TextAnalysisComponent,
    InterventionComponent,
    SimpleBotComponent,
    KnowledgeGraphComponent,
    OverviewComponent,
    LoginComponent,
    PrivacyComponent,
    WelcomeComponent,
    LrsAnalyticsComponent,
    BotAnalysisComponent,
    RocketChatComponent,
    LogoutComponent,
    MentorViewNavigationComponent,
    ResourceEditComponent,
    ResourceEditInterventionsComponent,
    ResourceMentorResultsComponent,
    InterventionEditComponent,
    ExamFeedbackEditComponent,
    HelpComponent,
    GetPeriodComponent,
    GetVisibilityComponent,
    WritingTaskEditComponent,
    SimpleBotEditComponent,
    UploadFileComponent,
    GetVarTextComponent,
    CircularProgressBarComponent,
    BotInfoTextsComponent,
    TextAnalysisHistoryComponent,
    TextAnalysisKnowledgeGraphComponent,
    SearchComponent,
    PopupSearchResultComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    OAuthModule.forRoot(),
    ChartsModule,
    FormsModule,
    AngularEditorModule,
    NgbModule
  ],
  providers: [
    MwbConfiguration,
    MwbConfigurationFromJson,
    HelpConfigurationFromJson,
    LoginService,
    UserInfo,
    { provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  entryComponents: [NgMentoringWorkbenchComponent]
})
export class AppModule {

  constructor(private injector: Injector, private router: Router, private location: Location) {
    this.router.navigateByUrl(this.location.path(true));
  }

  ngDoBootstrap() {
    const el = createCustomElement(NgMentoringWorkbenchComponent, {
      injector: this.injector
    });
    customElements.define('wb-card', el);
  }
}
