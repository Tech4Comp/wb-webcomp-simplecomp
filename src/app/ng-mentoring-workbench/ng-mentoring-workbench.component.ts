import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { LoginService } from "src/app/services/login-service";
import '@angular/common/locales/global/de';
import { environment } from 'src/environments/environment';
import { UserInfo } from 'src/app/model/userInfo';
import { Router } from '@angular/router';
import { WorkbenchService } from '../services/workbench.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-ng-mentoring-workbench',
  templateUrl: './ng-mentoring-workbench.component.html',
  styleUrls: ['./ng-mentoring-workbench.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NgMentoringWorkbenchComponent implements OnInit {
  @Input() maincolor: string;

  public version: string;
  public serviceVersion = 'Service not available';
  private logoutUrl = '/logout'

  constructor(
    private mwbService: WorkbenchService,
    private loginService: LoginService,
    public userInfo: UserInfo,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.version = environment.version;
    this.mwbService.getServiceVersion().subscribe(data => this.serviceVersion = data);
  }

  public isLoggedIn() {
    return this.loginService.hasAccess();
  }

  public logout() {
    this.router.navigate([this.logoutUrl], { queryParams: { redirect: this.router.routerState.snapshot.url }});
  }
}
