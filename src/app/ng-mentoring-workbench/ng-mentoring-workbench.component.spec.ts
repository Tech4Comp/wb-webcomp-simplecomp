import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NgMentoringWorkbenchComponent } from './ng-mentoring-workbench.component';

describe('NgMentoringWorkbenchComponent', () => {
  let component: NgMentoringWorkbenchComponent;
  let fixture: ComponentFixture<NgMentoringWorkbenchComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NgMentoringWorkbenchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgMentoringWorkbenchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
