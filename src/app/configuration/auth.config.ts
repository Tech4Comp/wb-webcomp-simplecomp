import { AuthConfig } from 'angular-oauth2-oidc';
import { environment } from '../../environments/environment';


export class CustomAuthConfig extends AuthConfig {
  // Url of the Identity Provider
  //old: issuer:string = 'https://api.learning-layers.eu/auth/realms/main';
  issuer:string = 'https://auth.las2peer.org/auth/realms/main';

  // URL of the SPA to redirect the user to after login
  prefixRedirectUri:string = environment.prefixRedirectUri;
  // URL of the SPA to redirect the user to after login/logout
  redirectUri:string = window.location.origin + '/index.html';

  // URL of the SPA to redirect the user after silent refresh
  silentRefreshRedirectUri:string = window.location.origin + '/silent-refresh.html';

  // defaults to true for implicit flow and false for code flow
  // as for code code the default is using a refresh_token
  // Also see docs section 'Token Refresh'
  useSilentRefresh: true;

  // use code flow
  responseType:string = 'code';

  dummyClientSecret:string = 'UZHXUzSvcI6jPzNF52AxlT9F-kFP_9wm0CjZJ_Ds2Wp6yOH6rqyH2O3mE5PzeLIB58s0EO6epFsqCxwmoB8EQw';

  // The SPA's id. The SPA is registered with this id at the auth-server
  clientId:string = 'd766e0f8-f8da-40ce-8bad-3c7ab0363b08';

  // set the scope for the permissions the client should request
  // The first three are defined by OIDC. The 4th is a usecase-specific one
  scope:string = 'openid profile email';


  constructor(resource:string, intervention:string) {
    super();
    if (resource) {
      this.redirectUri = this.prefixRedirectUri + "/resource/" + resource;
    }
    if (intervention) {
      this.redirectUri += "/intervention/" + intervention;
    }
  }
}
