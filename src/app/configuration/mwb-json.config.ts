import { Injectable } from '@angular/core';
import { JsonIntervention, JsonResource, MwbJson } from "../model/mwb-json";
import * as _mwb from "src/app/configuration/json/mwb.json";
import { Resource } from '../model/resource';
import { Intervention, InterventionType } from '../model/intervention';
import { ExamFeedback } from '../model/examFeedback';
import { TextAnalysis, WritingTask } from '../model/textAnalysis';
import { BotInfo } from '../model/botInfo';


@Injectable({
  providedIn: 'root'
})
export class MwbConfigurationFromJson {
  private mwb: MwbJson;
  private maintainedResources: (string | number)[] = ['mwb-dev'];
  private usedResources: (string | number)[] = ['mwb-dev'];

  constructor() {
    this.init();
  }

  init() {
    // load resource config
    this.mwb = <MwbJson>(_mwb as any).default;
    this.mwb.examFeedbacks = this.mwb.examFeedbacks || [];
    this.mwb.textAnalysis = this.mwb.textAnalysis || [];
    this.createExamFeedbackItems();
    this.createWritingTaskItems();
  }

  private createExamFeedbackItems()
  {
    let counter = 0;
    let self = this;
    this.mwb.interventions.forEach((intervention) => {
      if (intervention.type == InterventionType.ExamFeedback && intervention.examName) {
        let examFeedback = new ExamFeedback();
        counter++;
        examFeedback.id = intervention.id+'_'+counter;
        examFeedback.name = intervention.examName;
        examFeedback.interventionId = intervention.id;
        examFeedback.visibleFrom = intervention.date;
        examFeedback.contentBeforeExam = intervention.contentBefore;
        examFeedback.contentAfterExam = intervention.content;
        self.mwb.examFeedbacks.push(examFeedback);
      }
    });
  }

  private createWritingTaskItems()
  {
    let self = this;
    this.mwb.interventions.forEach(function (intervention) {
      if (intervention.type == InterventionType.TextAnalysis && intervention.tasks) {
        for (let counter = 1; counter <= intervention.tasks; counter++) {
          let writingTask = new WritingTask();
          writingTask.id = intervention.id+'_'+counter;
          writingTask.name = intervention.name+'_'+counter;
          writingTask.interventionId = intervention.id;
          writingTask.visibleFrom = intervention.date;
          self.mwb.textAnalysis.push(writingTask);
        }
      }
    });
  }

  private static createResourceFromJsonObject(resource: JsonResource) : JsonResource {
    // set fields not defined in the json representation to their default value
    let result = new JsonResource();
    for (let field in result) {
      result[field] = resource[field] || result[field];
    }
    return result;
  }

  private static createInterventionFromJsonObject(intervention: JsonIntervention) : JsonIntervention {
    // set fields not defined in the json representation to their default value
    let result = new JsonIntervention;
    for (let field in result) {
      result[field] = intervention[field] || result[field];
    }
    return result;
  }

  private static copyWritingTask(writingTask: WritingTask) : WritingTask {
    let result = new WritingTask();
    for (let field in result) {
      result[field] = writingTask[field];
    }
    return result;
  }

  private static copyFeedback(examFeedback: ExamFeedback) : ExamFeedback {
    let result = new ExamFeedback();
    for (let field in result) {
      result[field] = examFeedback[field];
    }
    return result;
  }

  private static jsonResourceToResource(json: JsonResource) : Resource {
    let resource = new Resource();
    for (let field in json) {
      switch (field) {
        case 'resourceIntro':
          resource.name = json[field];
          break;
        case 'resourceDetails':
          resource.description = json[field];
          break;
        default:
          resource[field] = json[field];
      }
    }
    return resource;
  }

  private static resourceToJsonResource(resource: Resource) : JsonResource {
    let json = new JsonResource();
    for (let field in resource) {
      switch (field) {
        case 'name':
          json.resourceIntro = resource[field];
          break;
        case 'description':
          json.resourceDetails = resource[field];
          break;
        default: 
          json[field] = resource[field];
      }
    }
    return json;
  }

  private jsonInterventionToIntervention(json: JsonIntervention) : Intervention
  {
    let intervention = new Intervention();
    intervention.resourceId = this.getResourceIdForIntervention(json.id);
    for (let field in json) {
      switch (field) {
        case 'intro':
          intervention.introText = json[field];
          break;
        case 'interventionHints':
          intervention.hints = json[field];
          break;
        case 'interventionHelp':
          intervention.helpText = json[field];
          break;
        case 'interventionFeedback':
          intervention.feedbackText = json[field];
          break;
        default:
          intervention[field] = json[field];
      }
    }
    return intervention;
  }

  private static interventionToJsonIntervention(intervention : Intervention) : JsonIntervention {
    let json = new JsonIntervention();
    for (let field in intervention) {
      switch (field) {
        case 'introText':
          json.intro = intervention[field]
          break;
        case 'hints':
          json.interventionHints = intervention[field]
          break;
        case 'helpText':
          json.interventionHelp = intervention[field]
          break;
        case 'feedbackText':
          json.interventionFeedback = intervention[field]
          break;
        default:
          json[field] = intervention[field] || json[field];
      }
    }
    return json;
  }

  private getJsonInterventionInfo(interventionId : string | number) : JsonIntervention {
    let intervention = this.mwb.interventions
      .find(x => interventionId === x.id);
    return intervention ? MwbConfigurationFromJson.createInterventionFromJsonObject(intervention) : new JsonIntervention();
  }

  public isValidResource(resourceId : string | number) : boolean {
    return this.getResourceInfo(resourceId).id != '';
  }

  private getResourceIdx(id: string) : number {
    return this.mwb.resources
      .findIndex(x => id === x.id);
  }

  private touchResource(id : string | number) {
    let idx = this.getResourceIdx(id as string);
    if (idx < 0)
      return;
    let resource = this.mwb.resources[idx];
    resource.lastChanged = Date.now();
    this.mwb.resources[idx] = resource;
  }

  public saveResource(resource: Resource) : boolean {
    let idx = this.getResourceIdx(resource.id);
    if (idx < 0)
      return false;
    resource.lastChanged = Date.now();
    this.mwb.resources[idx] = MwbConfigurationFromJson.resourceToJsonResource(resource);
    return true;
  }

  public mayCreateNewResource() : boolean {
    return true;
  }

  private getNewResourceId() : string {
    let ids = this.mwb.resources.map(x => Number(x.id))
      .filter(x => !isNaN(x))
      .sort((x, y) => x - y);
    return (ids.length > 0 ? ids[ids.length-1] + 1 : 1) + '';
  }

  public createResource() : string | number {
    let resource = new Resource();
    resource.id = this.getNewResourceId();
    resource.lastChanged = Date.now();
    this.mwb.resources.push(MwbConfigurationFromJson.resourceToJsonResource(resource));
    this.maintainedResources.push(resource.id);
    return resource.id;
  }

  public deleteResource(id : string | number) : boolean {
    this.mwb.resources = this.mwb.resources
      .filter(x => x.id != id);
    this.maintainedResources = this.maintainedResources
      .filter(x => x != id);
    this.usedResources = this.usedResources
      .filter(x => x != id);
    return this.mwb.resources.findIndex(x => x.id === id) < 0;
  }


  public getMaintainedResources() : (string | number)[]{
    return this.maintainedResources;
    // return this.mwb.resources.map(x => x.id);
  }

  public getUsedResources() : (string | number)[] {
    return this.usedResources;
    // return this.mwb.resources.map(x => x.id);
  }

  private getJsonResourceInfo(resourceId : string | number) : JsonResource {
    let resource = this.mwb.resources
      .find(x => resourceId === x.id);
    return resource ? 
      MwbConfigurationFromJson.createResourceFromJsonObject(resource) : 
      new JsonResource();
  }

  public getResourceInfo(resourceId : string | number) : Resource {
    let jsonResourceInfo = this.getJsonResourceInfo(resourceId);
    return MwbConfigurationFromJson.jsonResourceToResource(jsonResourceInfo);
  }


  public getInterventionInfo(interventionId : string | number) : Intervention {
    return this.jsonInterventionToIntervention(this.getJsonInterventionInfo(interventionId));
  }
  
  public isValidIntervention(interventionId : string | number) : boolean {
    return this.getInterventionInfo(interventionId).id != '';
  }

  public getInterventionsFor(resourceId : string | number) : Intervention[] {
    
    return this.getJsonResourceInfo(resourceId).interventionIds
      .map(x => this.getInterventionInfo(x));
  }

  private getInterventionIdx(id: string) : number {
    return this.mwb.interventions
      .findIndex(x => id === x.id);
  }

  public saveIntervention(intervention: Intervention) : boolean {
    let idx = this.getInterventionIdx(intervention.id);
    if (idx < 0)
      return false;
    intervention.lastChanged = Date.now();
    this.mwb.interventions[idx] = MwbConfigurationFromJson.interventionToJsonIntervention(intervention);
    this.touchResource(intervention.resourceId);
    return true;
  }

  private getNewInterventionId() : string {
    let ids = this.mwb.interventions.map(x => Number(x.id))
      .filter(x => !isNaN(x))
      .sort((x, y) => x - y);
    return (ids.length > 0 ? ids[ids.length-1] + 1 : 1) + '';
  }

  public createIntervention(resourceId: string | number, interventionType : InterventionType) : string {
    let intervention = new Intervention();
    intervention.type = interventionType;
    intervention.resourceId = resourceId as string;
    intervention.id = this.getNewInterventionId();
    intervention.lastChanged = Date.now();
    this.mwb.interventions.push(MwbConfigurationFromJson.interventionToJsonIntervention(intervention));
    this.mwb.resources.forEach(x => {
      if (x.id == resourceId) {
        x.interventionIds.push(intervention.id);
        x.lastChanged = Date.now();
      }
    });  
    return intervention.id;
  }

  public deleteIntervention(id : string | number) : boolean {
    this.touchResource(this.getInterventionInfo(id).resourceId);
    this.mwb.interventions = this.mwb.interventions
      .filter(x => x.id != id);

    this.mwb.resources.forEach(x => {
      x.interventionIds = x.interventionIds.filter(i => i != id);
    });  
    return this.mwb.interventions.findIndex(x => x.id === id) < 0;
  }

  private touchIntervention(id : string | number) {
    let idx = this.getInterventionIdx(id as string);
    if (idx < 0)
      return;
    let intervention = this.mwb.interventions[idx];
    intervention.lastChanged = Date.now();
    this.mwb.interventions[idx] = intervention;
    this.touchResource(this.getInterventionInfo(id).resourceId);
  }

  public getAllExamFeedbacks(interventionId : string | number) : ExamFeedback[] {
    return this.mwb.examFeedbacks
      .filter(x => x.interventionId == interventionId);
  }

  public getAllWritingTasks(interventionId : string | number) : WritingTask[] {
    return this.mwb.textAnalysis
      .filter(x => x.interventionId == interventionId);
  }

  public getInterventionItems(interventionId : string | number) : ExamFeedback[] | WritingTask[] {
    let interventionInfo = this.getInterventionInfo(interventionId); 
    switch (interventionInfo.type) {
      case InterventionType.ExamFeedback:
        return this.getAllExamFeedbacks(interventionId);
      case InterventionType.TextAnalysis:
        return this.getAllWritingTasks(interventionId);
    }
    return [];
  }

  public deleteExamFeedback(id : string | number) : boolean {
    let info = this.getExamFeedbackInfo(id);
    this.touchIntervention(info.id);
    this.mwb.examFeedbacks = this.mwb.examFeedbacks
      .filter(x => x.id != id);
    return this.mwb.examFeedbacks.findIndex(x => x.id === id) < 0;
  }

  private getNewExamFeedbackId() : string {
    let ids = this.mwb.examFeedbacks.map(x => Number(x.id))
      .filter(x => !isNaN(x))
      .sort((x, y) => x - y);
    return (ids.length > 0 ? ids[ids.length-1] + 1 : 1) + '';
  }

  public createExamFeedback(interventionId: string | number) : string | number {
    let examFeedback = new ExamFeedback();
    examFeedback.interventionId = interventionId as string;
    examFeedback.id = this.getNewExamFeedbackId();
    examFeedback.lastChanged = Date.now();
    this.mwb.examFeedbacks.push(examFeedback);
    this.touchIntervention(interventionId);
    return examFeedback.id;
  }

  public deleteWritingTask(id : string | number) : boolean {
    let info = this.getWritingTaskInfo(id);
    this.touchIntervention(info.id);
    this.mwb.textAnalysis = this.mwb.textAnalysis
      .filter(x => x.id != id);
    return this.mwb.textAnalysis.findIndex(x => x.id === id) < 0;
  }

  private getNewWritingTaskId() : string {
    let ids = this.mwb.textAnalysis.map(x => Number(x.id))
      .filter(x => !isNaN(x))
      .sort((x, y) => x - y);
    return (ids.length > 0 ? ids[ids.length-1] + 1 : 1) + '';
  }

  public createWritingTask(interventionId: string | number) : string {
    let writingTask = new WritingTask();
    writingTask.interventionId = interventionId as string;
    writingTask.id = this.getNewWritingTaskId();
    writingTask.lastChanged = Date.now();
    this.mwb.textAnalysis.push(writingTask);
    this.touchIntervention(interventionId);
    return writingTask.id;
  }

  public getMaintainedInterventionsFor(resourceId : string | number) : Intervention[] {
    return this.getInterventionsFor(resourceId);
  }

  public getResourceIdForIntervention(interventionId : string | number) {
    let resource = this.mwb.resources
      .find(x => x.interventionIds.find(id => id == interventionId));
    return resource ? resource.id : '';
  }

  public getBotInfo(interventionId : string | number) : BotInfo {
    let jsonIntervention = this.getJsonInterventionInfo(interventionId);
    let botInfo = new BotInfo();
    botInfo.id = jsonIntervention.id;
    botInfo.nameFramework = jsonIntervention.botnameFW;
    return botInfo;
  }

  public getBotNameForRocketChat(interventionId : string | number) : string {
    let jsonIntervention = this.getJsonInterventionInfo(interventionId);
    return jsonIntervention.botnameRocketChat;
  }

  public getTextAnalysisInfo(interventionId : string | number) : TextAnalysis {
    let jsonIntervention = this.getJsonInterventionInfo(interventionId);
    let writingTasks = new TextAnalysis();
    writingTasks.interventionId = jsonIntervention.id;
    writingTasks.lrsId = '';
    writingTasks.eraseWarning = jsonIntervention.eraseWarning;
    return writingTasks;
  }

  public getNumberOfWritingTasks(interventionId : string | number) : number {
    let jsonIntervention = this.getJsonInterventionInfo(interventionId);
    return jsonIntervention.tasks*1;
  }

  public getWritingTaskInfo(id : string | number) : WritingTask
  {
    let writingTask = this.mwb.textAnalysis
      .find(x => id === x.id);
    return writingTask ? MwbConfigurationFromJson.copyWritingTask(writingTask) : new WritingTask();
  }

  private getWritingTaskIdx(id: string) : number {
    return this.mwb.textAnalysis
      .findIndex(x => id === x.id);
  }

  public saveWritingTask(writingTask: WritingTask) : boolean {
    let idx = this.getWritingTaskIdx(writingTask.id);
    if (idx < 0)
      return false;
    writingTask.lastChanged = Date.now();
    this.mwb.textAnalysis[idx] = writingTask;
    this.touchIntervention(writingTask.interventionId);
    return true;
  }

  public getExamFeedbackInfo(id : string | number) : ExamFeedback
  {
    let examFeedback = this.mwb.examFeedbacks
      .find(x => id === x.interventionId);
    return examFeedback ? MwbConfigurationFromJson.copyFeedback(examFeedback) : new ExamFeedback();
  }

  private getExamFeedbackIdx(id: string) : number {
    return this.mwb.examFeedbacks
      .findIndex(x => id === x.id);
  }

  public saveExamFeedback(examFeedback: ExamFeedback) : boolean {
    let idx = this.getExamFeedbackIdx(examFeedback.id);
    if (idx < 0)
      return false;
    examFeedback.lastChanged = Date.now();
    this.mwb.examFeedbacks[idx] = examFeedback;
    this.touchIntervention(examFeedback.interventionId);
    return true;
  }
}
