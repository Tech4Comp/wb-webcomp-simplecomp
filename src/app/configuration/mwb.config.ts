import { Injectable } from "@angular/core";
import { MwbConfigurationFromJson } from "./mwb-json.config";

@Injectable({
  providedIn: 'root'
})
export class MwbConfiguration {
  constructor(
    public configuration : MwbConfigurationFromJson
  ) {}
}
