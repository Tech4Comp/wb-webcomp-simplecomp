import { AngularEditorConfig } from "@kolkov/angular-editor";

export function configEditor() : AngularEditorConfig {
  return {
    editable: true,
    spellcheck: false,
    height: '10em',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'no',
    enableToolbar: true,
    showToolbar: true,
    customClasses: [
      {
        name: 'grauer Text',
        class: 'grey-text'
      },
      {
        name: 'blauer Text',
        class: 'blue-text'
      },
      {
        name: 'roter Text',
        class: 'red-text'
      }
    ],
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      [
        'justifyLeft',
        'justifyCenter',
        'justifyRight',
        'justifyFull',
        'indent',
        'outdent',
        'fontName'
      ],
      [
        'fontSize',
        'textColor',
        'backgroundColor',
        'insertImage',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
        'toggleEditorMode'
      ]
    ]
  };
}
