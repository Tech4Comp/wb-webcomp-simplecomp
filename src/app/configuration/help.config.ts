import { Injectable } from "@angular/core";
import * as _help from "src/app/configuration/json/helpTexts.json";

@Injectable({
  providedIn: 'root'
})

class HelpJson {
  version: string;
  texts: HelpText[] = [];
}

export class HelpText {
  id: string;
  title: string;
  body: string;
}

export class HelpConfigurationFromJson {

  private help: HelpJson;

  constructor() {
    this.init();
  }

  init() {
    // load resource config
    this.help = <HelpJson>(_help as any).default;
  }


  public getHelpText(id : string) : HelpText {
    let helpText = this.help.texts
      .find(x => id === x.id);
    return helpText || new HelpText();
  }
}
