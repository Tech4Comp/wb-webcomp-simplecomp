import { Injectable } from '@angular/core';
import { OAuthService, UserInfo as UserProfile } from 'angular-oauth2-oidc';
import { CustomAuthConfig } from 'src/app/configuration/auth.config';
import { BehaviorSubject } from 'rxjs';
import { filter, tap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { redirectToUrlInQueryParams } from 'src/app/app-routing/guard';
import { UserInfo } from 'src/app/model/userInfo';

/* 
* see https://angular.de/artikel/oauth-odic-plugin/
* for additional information about using OAuth2 and OpenIdConnect
*/

@Injectable({ providedIn: 'root' })
export class LoginService {
  private isAuthenticatedSubject$ = new BehaviorSubject<boolean>(false);
  public isAuthenticated$ = this.isAuthenticatedSubject$.asObservable();

  private isDoneLoadingSubject$ = new BehaviorSubject<boolean>(false);
  public isDoneLoading$ = this.isDoneLoadingSubject$.asObservable();
  
  public isSetup = false;

  constructor(
    private oauthService: OAuthService,
    private userInfo: UserInfo,
    private router: Router,
    private route: ActivatedRoute
  ){} 

  public setupAuthentication(resId : string, intId : string) {
    if (this.isSetup)
      return;
    this.isSetup = true;
    this.initAuthServiceAndSubscribeToEvents(resId, intId);
    this.runInitialLogin();
  }

  private initAuthServiceAndSubscribeToEvents(resId : string, intId : string) {
    this.oauthService.configure(new CustomAuthConfig(resId, intId));
    this.oauthService.events
    .subscribe(_ => {
      this.isAuthenticatedSubject$.next(this.oauthService.hasValidAccessToken());
    });

    this.oauthService.events
      .pipe(filter(e => ['token_received'].includes(e.type)))
      .subscribe(e => this.setUserInfo());

    this.oauthService.events
      .pipe(filter(e => ['session_terminated', 'session_error'].includes(e.type)))
      .pipe(tap(_ => this.userInfo.unset()))
      .subscribe(e => this.navigateToWelcomePage());
  }

  private navigateToWelcomePage() {
    this.router.navigateByUrl('/welcome');
  }
    
  private runInitialLogin() {
    return this.oauthService.loadDiscoveryDocumentAndTryLogin()
      .then(() => {
        if (this.oauthService.hasValidAccessToken())
          this.oauthService.setupAutomaticSilentRefresh();
        else
          return Promise.reject();
      })
      .then(() => this.setUserInfo())
      .finally(() => {
        this.isDoneLoadingSubject$.next(true); 
      });
  }

  public login() {
    this.oauthService.initLoginFlowInPopup()
      .then(() => this.setUserInfo())
      .then(() => redirectToUrlInQueryParams(this.route, this.router));
  }

  public logout() {
    return this.oauthService.logOut();
  }

  public revokeAndLogout() {
    return this.oauthService.revokeTokenAndLogout();
  }

  public hasAccess() {
    return this.hasValidAccessToken() && this.hasValidIdToken();
  }

  public hasValidAccessToken() { 
    return this.oauthService.hasValidAccessToken(); 
  }

  public hasValidIdToken() { 
    return this.oauthService.hasValidIdToken(); 
  }

  public getAccessToken() {
    return this.hasAccess() ? this.oauthService.getAccessToken() : '';
  }

  public getAccessTokenExpiration() {
    return this.oauthService.getAccessTokenExpiration();
  }

  public loadUser() : Promise<object> {
    if (!this.hasValidIdToken())
      return Promise.reject();
    return this.oauthService.loadUserProfile()
  }

  private setUserInfo() : Promise<void> {
    return this.oauthService.loadUserProfile().then((userProfile) => {
      this.userInfo.setUserProfile(userProfile);
    });
  }
}