import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { UserInfo } from 'src/app/model/userInfo';
import { MwbConfiguration } from '../configuration/mwb.config';

@Injectable({ providedIn: 'root' })
export class PrivacyService {
  private addUserIdToCookieText = true;

  constructor(
    private userInfo : UserInfo,
    private mwb: MwbConfiguration,
    private cookieService: CookieService
  ) { } 

  public needsPrivacyStatement() {
    return this.mwb.configuration.isValidResource(this.userInfo.resourceId) ? true : false;
  }

  public agreePrivacyStatement() {
    if (this.needsPrivacyStatement())
      this.cookieService.set(this.getCookiePrivacyStatementName(), "true", null, null, null, true, "None");
  }

  public hasAgreedPrivacyStatement() : boolean {
    return this.cookieService.get(this.getCookiePrivacyStatementName()) == "true";
  }

  public removeAgreement() {
    this.cookieService.delete(this.getCookiePrivacyStatementName());
  }

  public deleteDeprecatedCookieVersions() {
    if (this.userInfo.resourceId === "ul-biwi5-ws22")
      this.cookieService.delete(this.getCookiePrivacyStatementName(0));
  }

  private getCurrentCookieVersion() {
    switch (this.userInfo.resourceId) {
      case "ul-biwi5-ws22":
        return 1;
    }
    return 0;
  }

  private getCookiePrivacyStatementName(version: number = -1) {
    version = version === -1 ? this.getCurrentCookieVersion() : version;
    var cookieName = "de-tech4comp-mwb-dev-" + version + "-privacy-statement-accepted";
    if (this.userInfo.resourceId)
      cookieName += "-"+this.userInfo.resourceId;
    if (this.addUserIdToCookieText && this.userInfo.getProfileData('sub'))
      cookieName += "-"+this.userInfo.getProfileData('sub');
    return cookieName;
  }
}