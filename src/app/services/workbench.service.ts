import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Record, SmallRecord } from "../model/record";
import { IntentDistribution } from "../model/intentDistribution";
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { LoginService } from './login-service';
import { UserInfo } from '../model/userInfo';
import { IntentPath } from '../model/intentPath';

@Injectable({
  providedIn: 'root'
})

export class WorkbenchService {
    private wbServiceUri: string;

    constructor(
      private httpClient: HttpClient,
      private loginService: LoginService,
      private userInfo: UserInfo
    )
    {
      this.wbServiceUri = environment.wbServiceUri;
    }

    private getAuthorizationHeader() {
      const accessToken = this.loginService.getAccessToken();
      const authorization = "Basic " + btoa(this.userInfo.name + ":" + this.userInfo.getProfileData("sub"));
      return new HttpHeaders({
        "authorization": authorization,
        "access-token": accessToken
      });
    }


    public getServiceVersion() {
      const httpOptions = {
        responseType: 'text' as const
      };
      return this.httpClient.get(
        this.wbServiceUri + "/version",
        httpOptions);
    }


    public getCntFulfilledTasks(interventionId) {
      const httpOptions = {
        headers: this.getAuthorizationHeader()
      };
      return this.httpClient.get(
        this.wbServiceUri + "/textAnalysis/cntFulfilled/" + interventionId,
        httpOptions);
    }

/*
    public getUserNotifications() {
       const httpOptions = {
        headers: this.getAuthorizationHeader()
      };
      return this.httpClient.get(
        this.wbServiceUri + "/notifications",
        httpOptions);
    }
*/
    public deleteUserdata() {
      const httpOptions = {
        headers: this.getAuthorizationHeader()
      };
      return this.httpClient.delete(
        this.wbServiceUri + "/userdata",
        httpOptions);
    }

    public getRecords(interventionId) {
      const httpOptions = {
        headers: this.getAuthorizationHeader()
      };
      return this.httpClient.get<Record[]>(
        this.wbServiceUri + "/textAnalysis/list/" + interventionId ,
        httpOptions);
    }

    public getRecordsForGraphTool(interventionId) {
      const httpOptions = {
        headers: this.getAuthorizationHeader()
      };
      return this.httpClient.get<SmallRecord[]>(this.wbServiceUri + "/get_Records_For_GraphTool/" + interventionId , httpOptions);
    }

    public getJsonGraphForRecord(interventionId, statementId) {
      const httpOptions = {
        responseType: 'text' as const,
        headers: this.getAuthorizationHeader()
      };
      return this.httpClient.get(this.wbServiceUri + "/get_jsonGraph/" + interventionId + "/" + statementId, httpOptions);
    }

    public getLastEntry(interventionId) {
      const httpOptions = {
        responseType: 'text' as const,
        headers: this.getAuthorizationHeader()
      };
      return this.httpClient.get(
        this.wbServiceUri + "/textAnalysis/timestampLastFileSent/" + interventionId ,
        httpOptions);
    }

    public getTextFile(interventionId, statementId, textType): Observable<Blob> {
      const httpOptions = {
        responseType: 'blob' as const,
        headers: this.getAuthorizationHeader()
      };
      return this.httpClient.get(
        this.wbServiceUri + "/get_SchreibText_from_LRS/" + interventionId + "/" + statementId + "/" + textType,
        httpOptions);
    }

    public getJsonFile(interventionId, statementId): Observable<Blob> {
      const httpOptions = {
        responseType: 'blob' as const,
        headers: this.getAuthorizationHeader()
      };
      return this.httpClient.get(
        this.wbServiceUri + "/get_TextJson_from_LRS/" + interventionId + "/" + statementId,
        httpOptions);
    }

    public hasTestResult(interventionId) {
      const httpOptions = {
        responseType: 'text' as const,
        headers: this.getAuthorizationHeader()
      };
      return this.httpClient.get(
        this.wbServiceUri + "/has_test_result/" + interventionId ,
        httpOptions);
    }

    public hasExamFeedback(interventionId) {
      const httpOptions = {
        responseType: 'text' as const,
        headers: this.getAuthorizationHeader()
      };
      return this.httpClient.get(
        this.wbServiceUri + "/has_exam_feedback/" + interventionId ,
        httpOptions);
    }

    public createExamFeedback(interventionId) {
      const httpOptions = {
        headers: this.getAuthorizationHeader()
      };
      return this.httpClient.get(
        this.wbServiceUri + "/create_exam_feedback_from_LRS/" + interventionId ,
        httpOptions);
    }

    public getTestFeedback(interventionId) {
      const httpOptions = {
        responseType: 'blob' as const,
        headers: this.getAuthorizationHeader()
      };
      return this.httpClient.get(
        this.wbServiceUri + "/get_exam_feedback_from_LRS/" + interventionId,
        httpOptions);
    }

    public deleteLRSitem(interventionId, itemId) {
      const httpOptions = {
        responseType: 'text' as const,
        headers: this.getAuthorizationHeader()
      };
      return this.httpClient.delete(
        this.wbServiceUri + "/delete_LRS_entry_and_log/" + interventionId + "/" + itemId,
        httpOptions);
    }

    public logKgUse(interventionId, actionsAsJsonA) {
      const httpOptions = {
        responseType: 'text' as const,
        headers: this.getAuthorizationHeader()
      };
      return this.httpClient.post(
        this.wbServiceUri + "/log_KG_use/" + interventionId, actionsAsJsonA,
        httpOptions);
    }

    public isBotOnline(botname) {
      const httpOptions = {
        responseType: 'text' as const,
      };
      return this.httpClient.get(this.wbServiceUri + "/is_bot_online/" + botname, httpOptions);
    }

    public getBotIntentDistribution(botName, analysisType, chosenSemester) {
      const httpOptions = {
        headers: this.getAuthorizationHeader()
      };
      var distinct;
      if (analysisType.endsWith("Distinct"))
        distinct = true;
      else
        distinct = false;
//      return this.httpClient.get<IntentDistribution[]>(this.wbServiceUri + "/get_intent_distribution/" + botName + "/" + time1 + "/" + time2 , httpOptions);
      return this.httpClient.get<IntentDistribution[]>(this.wbServiceUri + "/get_intent_distribution/" + botName + "?semester=" + chosenSemester + "&distinct=" + distinct, httpOptions);
    }

//    public getIntentPath(botName, time1, time2) {
    public getIntentPath(botName, chosenSemester) {
      const httpOptions = {
        headers: this.getAuthorizationHeader()
      };
//      return this.httpClient.get<IntentPath>(this.wbServiceUri + "/get_intent_path/" + botName + "/" + time1 + "/" + time2 , httpOptions);
      return this.httpClient.get<IntentPath>(this.wbServiceUri + "/get_intent_path/" + botName + "?semester=" + chosenSemester, httpOptions);
    }

    public getBotLogs(botName, chosenSemester, upto): Observable<any>{
      const httpOptions = {
        headers: this.getAuthorizationHeader(),
        responseType: 'text' as const
      };
      return this.httpClient.get(this.wbServiceUri + "/get_bot_logs/" + botName + "/" + upto + "?semester=" + chosenSemester, httpOptions);
    }



    public getSearchResources(searchId) {
      const httpOptions = {
        headers: this.getAuthorizationHeader(),
        responseType: 'json' as const
      };
      return this.httpClient.get(this.wbServiceUri + "/search/info/" + searchId, httpOptions);
    }

    public getSearchResults(interventionId, searchId, searchRequest) {
      const httpOptions = {
        headers: this.getSearchHeader(),
        responseType: 'json' as const
      };
      return this.httpClient.post(this.wbServiceUri+ "/search/" + searchId + "/intervention/" + interventionId, searchRequest, httpOptions);
    }

    private getSearchHeader() {
      const accessToken = this.loginService.getAccessToken();
      const authorization = "Basic " + btoa(this.userInfo.name + ":" + this.userInfo.getProfileData("sub"));
      return new HttpHeaders({
        "Content-Type":  "application/json",
        "authorization": authorization,
        "access-token": accessToken
      });
    }
  

}
