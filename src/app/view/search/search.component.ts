import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { popper } from '@popperjs/core';
import { MwbConfiguration } from 'src/app/configuration/mwb.config';
import { Intervention } from 'src/app/model/intervention';
import { SearchLearningResources } from 'src/app/model/searchLearningResources';
import { WorkbenchService } from 'src/app/services/workbench.service';
import { HelpComponent } from '../utils/help/help.component';
import { PopupSearchResultComponent } from '../utils/popup-search-result/popup-search-result.component';
import { cntPlusSingularPlural } from '../utils/utils';

@Component({
  selector: 'mwb-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  @Input() searchId: number | string;
  @ViewChild('help') help: HelpComponent; 
  @ViewChild('mwbPopupSearchResult') mwbPopupSearchResult: PopupSearchResultComponent; 

  private searchResultMaxSize = 500;

  public interventionInfo: Intervention;
  public searchInfo: SearchLearningResources;
  public searchFor = '';
  public searchedFor = '';
  public searchMainCategories: boolean[] = [];
  public searchCategories: boolean[] = [];
  public searchResults: any;
  public hasSearchResults: boolean = false;
  public showSearchExample: boolean = false; // current consent is that this info will influence students, so it is hidden right now
  public categoriesAssignedToMain: any[] = [];
  public isSearchAvailable: boolean = true;

  constructor(
    private mwb: MwbConfiguration,
    private workbenchService: WorkbenchService
  ) { }

  ngOnInit(): void {
    this.interventionInfo = this.mwb.configuration.getInterventionInfo(this.searchId);
    this.getSearchInfo();
  }

  private getSearchInfo() {
    this.workbenchService.getSearchResources(this.interventionInfo.name)
        .subscribe({
          next: (jsonData) => { this.processSearchInfoJson(jsonData) },
          error: () => { this.isSearchAvailable = false; }
        });
  }

  private processSearchInfoJson(json) {
    // todo: add try-clause
    let searchInfo = json[0];
    this.searchInfo = new SearchLearningResources();
    for (let field in this.searchInfo) {
      this.searchInfo[field] = searchInfo[field];
    }

    if (this.searchInfo.search_category) 
      this.searchCategories = this.searchInfo.search_category.map((x) => { return true; });
    if (this.searchInfo.main_category) {
      this.categoriesAssignedToMain = this.searchInfo.main_category.map((x) => { return x.search_categories; });
      this.searchInfo.main_category = this.searchInfo.main_category.map((x) => { return x.name; });
      this.searchMainCategories = this.searchInfo.main_category.map((x) => { return true; });
    }
  }


  public selectMainCategory(event, idx) {
    let self = this;
    this.categoriesAssignedToMain[idx].forEach(function (x) {
      self.searchCategories[x] = self.searchMainCategories[idx];
    });
  }

  public search() {
    this.searchFor = this.searchFor.trim();
    if (!this.searchFor) {
      this.help.popupInfo('Leere Suchanfrage','Bitte geben Sie eine Sucheinfrage ein', 'Info');
    }
    else if (this.searchInfo.search_category && !this.searchCategories.filter((x) => { return x; }).length) {
      this.help.popupInfo('Keine Suchkategorie gewählt','Bitte wählen Sie mindestens eine Suchkategorie aus', 'Info');
    }
    else
      this.getSearchResults();
   }

  private getSearchResults() {
    this.workbenchService.getSearchResults(this.interventionInfo.id, this.interventionInfo.name, this.buildSearchRequest())
        .subscribe({
          next: (data) => { 
            this.processSearchResults(data); 
          },
          error: (data) => { 
            console.log('error getSearchResults', data);
            this.help.popupInfo('Suche nicht verfügbar', 'Leider ist bei Ihrer Suchanfrage ein Fehler aufgetreten.<br>Bitte versuchen Sie es später noch einmal.', 'Fehler'); 
          }
        });
  }

  private buildSearchRequest() {
    let searchWord = this.searchFor
      .replace(new RegExp('\"', 'gi'), '"')
      .replace(new RegExp('"', 'gi'), '\\"');
    let reqBody = `{
      "indexName": "${this.searchInfo.index_name}",
      "searchWord": "${searchWord}",
      "size": ${this.searchResultMaxSize}
      ${this.buildSearchCategories()}
    }`;
    return reqBody;
  }

  private buildSearchCategories() {
    if (!this.searchInfo.search_category) 
      return "";
    let reqSearchCategories = this.searchCategories
      .map((x, idx) => { return x ? idx : -1; })
      .filter((x) => { return x >= 0; });
    let reqMainCategories = this.searchMainCategories
      .map((x, idx) => { return x ? idx : -1; })
      .filter((x) => { return x >= 0; });
    return `,
      "searchCategory": "${reqSearchCategories.join(', ')}",
      "mainCategory": "${reqMainCategories.map((x) => { return this.searchInfo.main_category[x]; }).join(', ')}",
      "category": "${reqSearchCategories.map((x) => { return this.searchInfo.search_category[x]; }).join(', ')}"
    `;
  }

  private processSearchResults(searchResults) {
    this.searchedFor = this.searchFor;
    this.formatSearchResults(searchResults);
    this.searchResults.slideList = this.searchResults.slides.map((x) => { 
      return this.getSearchResultDisplayName(x); 
    });
    this.searchResults.videoList = this.searchResults.videos.map((x) => { 
      return this.getSearchResultDisplayName(x); 
    });
    this.hasSearchResults = this.searchResults.slides.length || this.searchResults.videos.length;
  }

  private getSearchResultDisplayName(item) {
    let displayName = '<a href="#0">'+(item.category ? item.category+': ' : '')+item.name+'</a>'; 
    if (item.pages?.length)
      displayName += ' ('+cntPlusSingularPlural(item.pages.length,'Seite', 'Seiten')+')';
    if (item.locations?.length)
      displayName += ' ('+cntPlusSingularPlural(item.locations.length,'Fundstelle', 'Fundstellen')+')';
    return displayName;
  }

  private formatSearchResults(searchResults) {
    this.searchResults = { 
      'slides': this.formatSearchResultsForSlides(searchResults.slides), 
      'videos': this.formatSearchResultsForVideos(searchResults.videos)
    };
  }

  private extractName(nameWithSection: string) {
    // remove the part at the end of the string separated by a '_' from name (indicating the page/location)
    let name = nameWithSection.split('_');
    name.pop();
    return name.join('_');
  }

  private formatSearchResultsForSlides(slides) {
    let resultSlides = [];
    if (!slides)
      return resultSlides;
    let self = this;
    let currentSlide = null;
    for (let slideIdx in slides) {
      slides[slideIdx].forEach(function (item, idx) {
        if (idx === 0)
          currentSlide = {
            name: self.extractName(item.name),
            type: item.type,
            category: item.category,
            url: item.url,
            locations: [],
            pages: [],
          }
        if (item.paragraph)
          currentSlide.locations.push({
            start: item.start,
            end: item.end,
            paragraph: item.paragraph,
            score: item.score
          });
        else
          currentSlide.pages.push({
            page: item.start,
            score: item.score
          });
      });
      resultSlides.push(currentSlide);
    }
    return resultSlides;
  }

  private formatSearchResultsForVideos(videos) {
    let resultsVideos = [];
    if (!videos)
      return resultsVideos;
    let self = this;
    let currentVideo = null;
    let lastName = '';
    videos.forEach(function (video) {
      let currentName = self.extractName(video.name);
      let newParent = lastName !== currentName;
      if (currentVideo && newParent) { 
        resultsVideos.push(currentVideo);
      }
      if (!currentVideo || newParent) {
        currentVideo = {
          name: currentName,
          type: video.type,
          url: video.url,
          locations: []
        }
      }
      currentVideo.locations.push({
        score: video.score,
        start: video.start,
        end: video.end
      });
      lastName = currentName;
    });
    if (currentVideo) { 
      resultsVideos.push(currentVideo);
    }

    return resultsVideos;
  }

  public showSlideItem(event, idx) {
    event.preventDefault();
    let slide = this.searchResults.slides[idx];
    switch (slide.type) {
      case 'pdf':
      case 'txt':
        this.mwbPopupSearchResult.popupSearchResult(this.searchFor, slide);
        break;
    }
  }

  public showVideoItem(event, idx) {
    event.preventDefault();
    let video = this.searchResults.videos[idx];
    switch (video.type) {
      case 'video':
        this.mwbPopupSearchResult.popupSearchResult(this.searchFor, video);
        break;
    }
  }

}
