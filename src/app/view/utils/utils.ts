export function forceName(name: string) : string {
  return name || 'noch kein Name angegeben';
}

export function singularPlural(cnt: number, singular: string, plural: string) : string {
  return cnt == 1 ? singular : plural;
}

export function cntPlusSingularPlural(cnt: number, singular: string, plural: string) : string {
  return cnt+' '+singularPlural(cnt, singular, plural);
}