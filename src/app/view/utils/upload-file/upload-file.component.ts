import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.css']
})
export class UploadFileComponent {
  @Input() fileName = '';
  @Input() accept = '.text, .txt, .pdf';
  @Output('fileNameChange') changeFileName: EventEmitter<string>;
  private file : File;
  public mayUpload = false;

  constructor(
    private http: HttpClient
  ) {
    this.changeFileName = new EventEmitter<string>();
  }
  
  onFileSelected(event) {
    this.file = event.target.files[0];
    if (this.file) {
      this.fileName = this.file.name;
      this.changeFileName.emit(this.fileName);
      this.mayUpload = true;
    }
  }

  uploadFile() {
    // just an example for uploading the file using FormData
    const enableUpload = false;
    if (this.file) {
      const formData = new FormData();
      formData.append("upload", this.file);
      const uploadUrl = 'http://mydomain.org/upload';
      const upload$ = this.http.post(uploadUrl, formData);
      if (enableUpload)
        upload$.subscribe(() => {
          this.mayUpload = false;
        });
      else
        this.mayUpload = false;
    }
  }
}
