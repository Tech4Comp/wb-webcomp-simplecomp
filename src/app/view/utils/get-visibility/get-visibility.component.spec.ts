import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetVisibilityComponent } from './get-visibility.component';

describe('GetVisibilityComponent', () => {
  let component: GetVisibilityComponent;
  let fixture: ComponentFixture<GetVisibilityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetVisibilityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetVisibilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
