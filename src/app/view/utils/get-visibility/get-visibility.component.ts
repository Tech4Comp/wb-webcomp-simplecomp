import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-get-visibility',
  templateUrl: './get-visibility.component.html',
  styleUrls: ['./get-visibility.component.css']
})

export class GetVisibilityComponent implements OnInit {
  @Input() visibility = 0;
  @Output('visibilityChange') changeVisibilityEvent: EventEmitter<number>;
  @Input() from = 0;
  @Output('fromChange') changeFromEvent: EventEmitter<number>;
  @Input() to = 0;
  @Output('toChange') changeToEvent: EventEmitter<number>;
  public visibilityOption = '';

  constructor() { 
    this.changeVisibilityEvent = new EventEmitter<number>();
    this.changeFromEvent = new EventEmitter<number>();
    this.changeToEvent = new EventEmitter<number>();
  }

  ngOnInit(): void {
    this.visibilityOption = ''+this.visibility;
  }

  public changeVisibility() {
    this.changeVisibilityEvent.emit(Number(this.visibilityOption));
  }

  public changePeriod(isValid: boolean) {
    if (isValid) {
      this.changeFromEvent.emit(this.from);
      this.changeToEvent.emit(this.to);
    }
  }
}
