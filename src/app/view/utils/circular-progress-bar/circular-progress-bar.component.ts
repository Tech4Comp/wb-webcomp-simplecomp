import { ThisReceiver } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-circular-progress-bar',
  templateUrl: './circular-progress-bar.component.html',
  styleUrls: ['./circular-progress-bar.component.css']
})
export class CircularProgressBarComponent implements OnInit {
  @Input() total: number;
  @Input() progress: number;

  constructor() { }

  ngOnInit(): void {
  }

  public percentage(progress, total) : number {
    return Math.round(100*progress/total);
  }

  public over50(progress, total) : boolean {
    return this.percentage(progress,total) > 50;
  }
}
