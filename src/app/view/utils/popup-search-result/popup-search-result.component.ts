import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'mwb-popup-search-result',
  templateUrl: './popup-search-result.component.html',
  styleUrls: ['./popup-search-result.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PopupSearchResultComponent implements OnInit {
  @ViewChild('popupContent') popupContent: ElementRef;
  @ViewChild('iframe') iframe: ElementRef;
  @ViewChild('videoSrc') videoSrc: ElementRef;
  public basicSearchUrl = 'https://tech4comp-search.dfki.de';
  public searchTerm: string = '';
  public pages: any[];
  public videoLocations: any[];
  public iframeUrl: SafeResourceUrl = '';
  public videoUrl: SafeResourceUrl = '';
  public txtContent: string = '';
  private item = null;

  constructor(
    private httpClient: HttpClient,
    private modalService: NgbModal,
    private sanitizer: DomSanitizer
  ) { }
  
  ngOnInit(): void {
  }

  private popup() {
    this.modalService.open(this.popupContent, { fullscreen: true });
  }

  private getUrlForItem() {
    return this.basicSearchUrl+'/'+this.item.url;
  }

  private getUrlForPage(page) {
    return this.getUrlForItem()+'#search='+this.searchTerm+'&page='+page;
  }

  private getVideoUrlForLocation(location) {
    return this.getUrlForItem()+'#t='+location.start+','+location.end;
  }

  public popupSearchResult(searchTerm: string, item) {
    this.resetParams();
    this.searchTerm = searchTerm.trim();
    this.item = item;
    console.log(this.item);
    if (item.pages)
      this.pages = item.pages.map(function(x) { return x.page; });
    switch (this.item.type) {
      case 'pdf':
        this.setIframeUrl(this.pages[0]);
        break;
      case 'txt':
        this.loadTxtContent();
        break;
      case 'video':
        if (item.locations?.length) {
          let self = this;
          this.videoLocations = item.locations.map(function(x) { return self.displayTimestamp(x.start)+'-'+self.displayTimestamp(x.end); });
          this.setVideoUrl(0);
        }
        break;
    }
    this.popup();
  }

  private displayTimestamp(timestamp) {
    let min = Math.floor(timestamp/60);
    let sec = timestamp-(min*60);
    return min+':'+(sec<9 ? '0' : '')+sec;
  }

  private resetParams() {
    this.item = null;
    this.pages = [];
    this.videoLocations = [];
    this.txtContent = '';
  }

  public setIframeUrl(page) {
    this.iframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.getUrlForPage(page));
  };

  public dummyLinkFor(item, index) {
    return (index ? ', ' : '')+'<a href="#0">'+item+'</a>';
  }

  public showPage(event, page) {
    event.preventDefault();
    this.setIframeUrl(page);
  }

  public hasPages() {
    return this.pages.length > 0;
  }

  public hasIFrame() {
    return this.item.type === 'pdf';
  }

  public hasLocations() {
    return this.item.locations.length > 0;
  }

  public hasVideo() {
    return this.videoLocations.length > 0;
  }

  public hasVideoLocations() {
    return this.videoLocations?.length > 0;
  }

  public loadTxtContent() {
    this.httpClient.get(this.getUrlForItem(), { responseType: 'text' as 'json'}).subscribe((data : string) => {
      this.txtContent = data.replace(new RegExp(this.searchTerm, 'gi'), '<span class="highlight">'+this.searchTerm+'</span>');
    })
  }

  public setVideoUrl(locationIdx) {
    let location = this.item.locations[locationIdx];
    let url = this.sanitizer.bypassSecurityTrustResourceUrl(this.getVideoUrlForLocation(location));
    this.videoUrl = url;
    let video = document.getElementById("videoTag") as HTMLVideoElement;
    if (video)
      video.load();
  }

  public showVideoLocation(event, locationIdx) {
    event.preventDefault();
    this.setVideoUrl(locationIdx);
  }

  public onIframeLoad() {
    console.log('iframe loaded');
  }

  public onVideoLoad() {
    console.log('video loaded');
  }

}
