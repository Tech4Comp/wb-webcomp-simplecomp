import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupSearchResultComponent } from './popup-search-result.component';

describe('PopupSearchResultComponent', () => {
  let component: PopupSearchResultComponent;
  let fixture: ComponentFixture<PopupSearchResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopupSearchResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupSearchResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
