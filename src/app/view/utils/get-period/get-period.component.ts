import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbDateStruct, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-get-period',
  templateUrl: './get-period.component.html',
  styleUrls: ['./get-period.component.css']
})
export class GetPeriodComponent implements OnInit {
  @Input() from = 0;
  @Output('fromChange') changeFromEvent: EventEmitter<number>;
  @Input() to = 0;
  @Output('toChange') changeToEvent: EventEmitter<number>;
  public dateFrom: NgbDateStruct;
  public dateTo: NgbDateStruct;
  public startDateForDatePicker: NgbDateStruct;
  public timeTo: NgbTimeStruct;
  public timeFrom: NgbTimeStruct;
  public error = '';

  constructor() { 
    this.changeFromEvent = new EventEmitter<number>();
    this.changeToEvent = new EventEmitter<number>();
  }

  ngOnInit(): void {
    this.setDateAndTime();
  }

  private setDateAndTime() {
    this.dateFrom = this.timestampToDate(this.from);
    this.timeFrom = this.timestampToTime(this.from);
    this.dateTo = this.timestampToDate(this.to || this.from);
    this.timeTo = this.timestampToTime(this.to || this.from);
    this.startDateForDatePicker = this.timestampToDate(this.from);
  }

  public changeFrom() {
    this.checkData();
    this.changeFromEvent.emit(this.getFrom());
  }

  public changeTo() {
    this.checkData();
    this.changeToEvent.emit(this.getTo());
  }

  public leave() {
  }

  public checkData() {
    this.error = '';
    if (!this.isValidDate(this.dateFrom))
      this.error = 'Von: ungültiges Datum';
    else if (!this.isValidTime(this.timeFrom))
      this.error = 'Beginn: ungültige Uhrzeit';
    else if (!this.isValidDate(this.dateTo))
      this.error = 'Bis: ungültiges Datum';
    else if (!this.isValidTime(this.timeTo))
      this.error = 'Ende: ungültige Uhrzeit';
    else if (this.getTo() < this.getFrom())
      this.error = 'Ende des Zeitraums liegt vor Beginn';
  }

  public isValid() {
    this.checkData();
    return this.error == '';
  }

  public getData() {
    return { from: this.getFrom(), to: this.getTo() };
  }

  public getFrom() : number {
    return this.isValidDate(this.dateFrom) && this.isValidTime(this.timeFrom) ? 
      this.dateTimeToTimestamp(this.dateFrom, this.timeFrom).valueOf() :
      0;
  }

  public getTo() : number {
    return this.isValidDate(this.dateTo) && this.isValidTime(this.timeTo) ? 
      this.dateTimeToTimestamp(this.dateTo, this.timeTo).valueOf() :
      0;
  }
  
  private timestampToDate(timestamp: number) : NgbDateStruct{
    let date = new Date(timestamp*1);
    return { day: date.getDate(), month: date.getMonth()+1, year: date.getFullYear()};
  }

  private timestampToTime(timestamp: number) : NgbTimeStruct {
    if (!timestamp) 
      return { hour: 0, minute: 0, second: 0 };
    let date = new Date(timestamp*1);
    return { hour: date.getHours(), minute: date.getMinutes(), second: 0 };
  }

  private isValidTime(time: NgbTimeStruct) : boolean {
    return !isNaN(new Date(2000, 0, 1, time.hour, time.minute).valueOf());
  }

  private isValidDate(date: NgbDateStruct) : boolean {
    return !isNaN(new Date(date.year, date.month-1, date.day).valueOf());
  }

  private dateTimeToTimestamp(date: NgbDateStruct, time: NgbTimeStruct) {
    return new Date(date.year, date.month-1, date.day, time.hour, time.minute);
  }

  public getStartDateForDatePicker() {
    let timestamp = this.from;
    return timestamp | Date.now();
  }

}
