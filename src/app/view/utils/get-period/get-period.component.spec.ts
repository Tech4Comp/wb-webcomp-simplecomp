import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetPeriodComponent } from './get-period.component';

describe('GetPeriodComponent', () => {
  let component: GetPeriodComponent;
  let fixture: ComponentFixture<GetPeriodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetPeriodComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetPeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
