import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { configEditor } from 'src/app/configuration/editor.config';
import { VarText } from 'src/app/model/varText';

/* IMPORTANT: 
  VarText properties are currently not completely supported, because
  there is no interface for reading and writing those values
  Please adapt this component as soon as the DB structure for handling varTexts is done (especially
  for type 'file')
  Depending on the implementation it might be necessary to adjust the component upload-file as well
*/

@Component({
  selector: 'app-get-var-text',
  templateUrl: './get-var-text.component.html',
  styleUrls: ['./get-var-text.component.css']
})
export class GetVarTextComponent implements OnInit {
  @Input() varText : VarText;
  @Output('varTextChange') changeVarText: EventEmitter<VarText>;
  public editorConfig: AngularEditorConfig;
  public varTextOption = 'none';
  public varTextText = '';
  public varTextUrl = '';
  public varTextFileName = '';
  
  constructor() { 
    this.changeVarText = new EventEmitter<VarText>();
  }

  ngOnInit(): void {
    this.varText = this.varText || new VarText();
    this.varTextOption = this.setVarTextOption();
    this.setVarTextValue();
    this.editorConfig = configEditor();
  }

  private setVarTextOption() : string {
    return this.varText.value ? this.varText.type : 'none';
  }

  private setVarTextValue() {
    switch (this.varTextOption) {
      case 'text':
        this.varTextText = this.varText.value as string;
        break;
      case 'url':
        this.varTextUrl = this.varText.value as string;
        break;
      case 'file':
        this.varTextFileName = this.varText.value as string;
        break;
    }
  }

  private getVarTextValue() {
    switch (this.varTextOption) {
      case 'text':
        return this.varTextText;
      case 'url':
        return this.varTextUrl;
      case 'file':
        return this.varTextFileName;
    }
    return '';
  }

  private getVarTextOption() {
    switch (this.varTextOption) {
      case 'none':
        return 'text';
    }
    return this.varTextOption;
  }

  public emitValue() {
    this.varText.type = this.getVarTextOption();
    this.varText.value = this.getVarTextValue();
    this.changeVarText.emit(this.varText);
  }
}
