import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetVarTextComponent } from './get-var-text.component';

describe('GetVarTextComponent', () => {
  let component: GetVarTextComponent;
  let fixture: ComponentFixture<GetVarTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetVarTextComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetVarTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
