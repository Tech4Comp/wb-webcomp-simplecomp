import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HelpConfigurationFromJson } from 'src/app/configuration/help.config';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css']
})
export class HelpComponent implements OnInit {
  @ViewChild('helpContent') helpContent: ElementRef;
  public content: string = '';
  public content2: string = '';
  public windowTitle: string = '';
  public confirm = false;

  constructor(
    private modalService: NgbModal,
    private helpJson: HelpConfigurationFromJson
  ) { }

  ngOnInit(): void {
  }

  private popup() {
    this.confirm = false;
    this.modalService.open(this.helpContent);
  }

  private confirmPopup() {
    this.confirm = true;
    return this.modalService.open(this.helpContent);
  }

  private formatBodyAndTitle(title: string, body: string) {
    return (title ? '<h3>'+title+'</h3>' : '') + '<p>'+body+'</p>';
  }

  public popupHelp(ids : string[]) {
    this.windowTitle = 'Hilfe';
    this.content = (ids|| [])
      .filter(id => id)
      .map(x => this.helpJson.getHelpText(x))
      .filter(help => help.body)
      .map(help => this.formatBodyAndTitle(help.title, help.body))
      .join('');
      this.content2 = '';
      this.popup();
  }

  public popupInfo(title: string, body: string, windowTitle: string = '') {
    this.windowTitle = windowTitle || 'Info';
    this.content = this.formatBodyAndTitle(title, body);
    this.content2 = '';
    this.popup();
  }

  public popupSave(result: boolean, content: string = '') {
    this.windowTitle = 'Speichern';
    this.content = content;
    this.content2 = result ? 'Erfolgreich gespeichert' : 'Speichern fehlgeschlagen';
    this.popup();
  }

  public popupDelete(result: boolean, content: string = '') {
    this.windowTitle = 'Löschen';
    this.content = content;
    this.content2 = result ? 'Erfolgreich gelöscht' : 'Löschen fehlgeschlagen';
    this.popup();
  }

  public confirmCreate(content: string = '') : Promise<any> {
    this.windowTitle = 'Neuen Eintrag erstellen';
    this.content = content;
    this.content2 = 'Eintrag wirklich erstellen?';
    return this.confirmPopup().result;
  }

  public confirmDelete(content: string = '') : Promise<any> {
    this.windowTitle = 'Löschen';
    this.content = content;
    this.content2 = 'Eintrag wirklich löschen?';
    return this.confirmPopup().result;
  }
}
