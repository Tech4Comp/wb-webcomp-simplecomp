import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceEditInterventionsComponent } from './resource-edit-interventions.component';

describe('ResourceEditInterventionsComponent', () => {
  let component: ResourceEditInterventionsComponent;
  let fixture: ComponentFixture<ResourceEditInterventionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResourceEditInterventionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceEditInterventionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
