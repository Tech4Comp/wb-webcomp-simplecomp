import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MwbConfiguration } from 'src/app/configuration/mwb.config';
import { Intervention, getValidInterventionTypes, getLongTextForInterventionType, InterventionType, getEnumInterventionType, isVisibleInterventionItem, getShortTextForInterventionType } from 'src/app/model/intervention';
import { Resource } from 'src/app/model/resource';
import { UserInfo } from 'src/app/model/userInfo';
import { HelpComponent } from '../../utils/help/help.component';
import { forceName } from '../../utils/utils';

@Component({
  selector: 'app-resource-edit-interventions',
  templateUrl: './resource-edit-interventions.component.html',
  styleUrls: ['./resource-edit-interventions.component.css']
})
export class ResourceEditInterventionsComponent implements OnInit {
  @ViewChild('help') help: HelpComponent ; 
  public selected = "interventions";
  public resourceId: string | number;
  public resourceInfo: Resource;
  public isValidResource: boolean;
  public interventionTypes: string[];
  public interventions: Intervention[];
  private expanded = {};

  constructor(
    private mwb: MwbConfiguration,
    private userInfo: UserInfo,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.interventionTypes = getValidInterventionTypes()
      .filter(x  => this.isSupportedInterventionTypeForEdit(x));
    this.resourceId = this.userInfo.resourceId;
    this.resourceInfo = this.mwb.configuration.getResourceInfo(this.resourceId);
    this.isValidResource = this.mwb.configuration.isValidResource(this.resourceId);
    this.loadInterventions();
  }

  private loadInterventions() {
    this.interventions = this.mwb.configuration.getMaintainedInterventionsFor(this.resourceId)
      .sort((x, y) => y.lastChanged - x.lastChanged);
    this.expanded = {};
    this.interventions.forEach(x => {
      if (this.mwb.configuration.getInterventionItems(x.id).length) {
        this.expanded[x.id] = false;
      }
    });  
  }

  private isSupportedInterventionTypeForEdit(type : string) {
    switch (InterventionType[type]) {
      case InterventionType.TextAnalysis:
      case InterventionType.ExamFeedback:
      case InterventionType.SimpleBot:
        return true;
    }
    return false;
  }

  public getLongText(type) {
    return getLongTextForInterventionType(type, true);
  }

  public showHelpForInterventionTypes() {
    let helpIds = ["textAnalysis", "examFeedback"];
    this.help.popupHelp(helpIds);
  }

  public forceName(name: string) :string {
    return forceName(name);
  }

  public mayExpand(interventionId: string | number) {
    return this.expanded[interventionId] !== undefined;
  }

  public isExpanded(interventionId: string | number) {
    return this.expanded[interventionId];
  }

  public toggle(interventionId: string | number) {
    this.expanded[interventionId] = !this.expanded[interventionId];
  }

  public getCollapsedStatus(interventionId: string | number) {
    if (!this.mayExpand(interventionId))
      return '';
    return this.isExpanded(interventionId) ? 'isExpanded' : 'isCollapsed';
  }

  public getInterventionItems(interventionId: string | number) {
    return this.mwb.configuration.getInterventionItems(interventionId)
      .sort((x, y) => y.lastChanged-x.lastChanged);
  }

  public getInterventionEnumType(type: string) {
    return getEnumInterventionType(type);
  }

  public createIntervention(interventionType : InterventionType) {
    let createText = 'Erstelle neue Intervention '+ getShortTextForInterventionType(interventionType, true);
    this.help.confirmCreate(createText).then(() => {
      let newInterventionId = this.mwb.configuration.createIntervention(this.resourceId, InterventionType[interventionType]);
      this.router.navigate(['/mentor/intervention/', newInterventionId]);
    })    
    .finally(() => {});
  }

  public deleteIntervention(intervention : Intervention) {
    let deleteText = 'Lösche Intervention <i>'+intervention.name + '</i><br/><br/>' +
      'Wann ist das erlaubt?<br/>'+
      'Welche Konsequenzen hat das?<br/>'+
      'Müssten die Nutzer von der Löschung ihrer Daten unterrichtet werden?';
    this.help.confirmDelete(deleteText).then(() => {
      let result = false;
      result = this.mwb.configuration.deleteIntervention(intervention.id);
      this.loadInterventions();
      this.help.popupDelete(result);
    })    
    .finally(() => {});
  }

  public getVisibilityForItem(intervention : Intervention, interventionItem) {
    switch (intervention.type) {
      case InterventionType.ExamFeedback:
      case InterventionType.TextAnalysis:
        return isVisibleInterventionItem(interventionItem) ?
        'sichtbar' : 
        'nicht sichtbar';
    }
    return '';
  }

}
