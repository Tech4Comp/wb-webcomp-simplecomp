import { Component, OnInit } from '@angular/core';
import { MwbConfiguration } from 'src/app/configuration/mwb.config';
import { Resource } from 'src/app/model/resource';
import { UserInfo } from 'src/app/model/userInfo';

@Component({
  selector: 'app-resource-mentor-results',
  templateUrl: './resource-mentor-results.component.html',
  styleUrls: ['./resource-mentor-results.component.css']
})
export class ResourceMentorResultsComponent implements OnInit {
  public selected = "results";
  public resourceId: string | number;
  public resourceInfo: Resource;

  constructor(
    private mwb: MwbConfiguration,
    private userInfo: UserInfo
  ) { }

  ngOnInit(): void {
    this.resourceId = this.userInfo.resourceId;
    this.resourceInfo = this.mwb.configuration.getResourceInfo(this.resourceId);
 }

}
