import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceMentorResultsComponent } from './resource-mentor-results.component';

describe('ResourceMentorResultsComponent', () => {
  let component: ResourceMentorResultsComponent;
  let fixture: ComponentFixture<ResourceMentorResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResourceMentorResultsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceMentorResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
