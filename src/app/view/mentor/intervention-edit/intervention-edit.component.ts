import { Component, OnInit, ViewChild } from '@angular/core';
import { MwbConfiguration } from 'src/app/configuration/mwb.config';
import { Intervention, IsInterventionWithDetails, GetInterventionItemsName as getInterventionItemsName, getShortTextForInterventionType, InterventionType, getEnumInterventionType, isVisibleInterventionItem, IsInterventionWithBot } from 'src/app/model/intervention';
import { Resource } from 'src/app/model/resource';
import { UserInfo } from 'src/app/model/userInfo';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { ExamFeedback } from 'src/app/model/examFeedback';
import { WritingTask } from 'src/app/model/textAnalysis';
import { HelpComponent } from '../../utils/help/help.component';
import { configEditor } from 'src/app/configuration/editor.config';
import { Router } from '@angular/router';
import { forceName } from '../../utils/utils';
import { BotInfo } from 'src/app/model/botInfo';

@Component({
  selector: 'app-intervention-edit',
  templateUrl: './intervention-edit.component.html',
  styleUrls: ['./intervention-edit.component.css']
})
export class InterventionEditComponent implements OnInit {
  @ViewChild('help') help: HelpComponent ; 
  public resourceId: string;
  public resourceInfo: Resource;
  public isValidResource: boolean;
  public interventionId: string;
  public interventionInfo: Intervention;
  public isValidIntervention: boolean;
  public interventionItems: ExamFeedback[] | WritingTask[];
  public editorConfig: AngularEditorConfig;
  public interventionItemsName: string;
  public interventionEnumType: string;
  public isTextAnalysisIntervention : boolean;
  public hasDetails = false;
  public botInfo: BotInfo;

  constructor(
    private mwb: MwbConfiguration,
    private userInfo: UserInfo,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.interventionId = this.userInfo.interventionId;
    this.interventionInfo = this.mwb.configuration.getInterventionInfo(this.interventionId);
    this.resourceId = this.interventionInfo.resourceId;
    this.resourceInfo = this.mwb.configuration.getResourceInfo(this.resourceId);
    this.isValidResource = this.mwb.configuration.isValidResource(this.resourceId);
    this.isValidIntervention = this.mwb.configuration.isValidIntervention(this.interventionId);
    if (IsInterventionWithBot(this.interventionInfo.type))
      this.botInfo = this.mwb.configuration.getBotInfo(this.interventionId);
    this.isTextAnalysisIntervention = this.interventionInfo.type == InterventionType.TextAnalysis;
    this.loadInterventionItems();
    this.interventionItemsName = getInterventionItemsName(this.interventionInfo.type, false);
    this.interventionEnumType = getEnumInterventionType(this.interventionInfo.type);
    this.editorConfig = configEditor();
  }

  private loadInterventionItems() {
    this.interventionItems = this.getDetailRecords()
      .sort((x, y) => y.lastChanged - x.lastChanged);
    this.hasDetails = this.interventionItems.length > 0;
  }

  private getDetailRecords() {
    switch (this.interventionInfo.type) {
      case InterventionType.ExamFeedback:
        return this.mwb.configuration.getAllExamFeedbacks(this.interventionId);
      case InterventionType.TextAnalysis:
        return this.mwb.configuration.getAllWritingTasks(this.interventionId);
    }
    return [];
  }

  public forceName(name: string) :string {
    return forceName(name);
  }

  public getVisibilityForItem(interventionItem) {
    switch (this.interventionInfo.type) {
      case InterventionType.ExamFeedback:
      case InterventionType.TextAnalysis:
        return isVisibleInterventionItem(interventionItem) ?
        'sichtbar' : 
        'nicht sichtbar';
    }
    return '';
  }

  public getShortText() {
    return getShortTextForInterventionType(this.interventionInfo.type);
  }

  public IsInterventionWithDetails() {
    return IsInterventionWithDetails(this.interventionInfo.type);
  }

  public IsInterventionWithBot() {
    return ;
  }

  public showHelpForIntervention() {
    this.help.popupHelp([this.interventionInfo.type]);
  }

  public showHelpForDescriptionText() {
    this.help.popupInfo('Intervention','Hilfe Beschreibung', 'Hilfe');
  }

  public showHelpForWelcomeText() {
    this.help.popupInfo('Intervention','Hilfe Willkommenstext', 'Hilfe');
  }

  public showHelpForHints() {
    this.help.popupInfo('Intervention','Hilfe Hinweistexte', 'Hilfe');
  }

  public showHelpForEraseWarning() {
    this.help.popupInfo('Intervention','Hilfe Löschhinweis', 'Hilfe');
  }

  public save() {
    let result = this.mwb.configuration.saveIntervention(this.interventionInfo);
    this.help.popupSave(result);
  }

  public createInterventionItemText() {
    return 'Erstelle '+ this.interventionItemsName + ' für '+ '\''+this.forceName(this.interventionInfo.name)+'\'';
  }

  public createInterventionItem() {
    this.help.confirmCreate(this.createInterventionItemText()).then(() => {
      let newInterventionItemId : string | number = '';
      switch (this.interventionInfo.type) {
        case InterventionType.ExamFeedback: 
          newInterventionItemId = this.mwb.configuration.createExamFeedback(this.interventionInfo.id);
          break;
        case InterventionType.TextAnalysis: 
          newInterventionItemId = this.mwb.configuration.createWritingTask(this.interventionInfo.id);
          break;
        default:
          this.help.popupInfo('invalid operation', 'create '+ this.interventionInfo.type + ' not yet supported');
          return;
      }
      this.router.navigate(['/mentor/', this.interventionEnumType, newInterventionItemId]);
    })    
    .finally(() => {});
  }

  public deleteInterventionItem(interventionItem, type : string) {
    let deleteText = 'Lösche '+getInterventionItemsName(this.interventionInfo.type)+' <i>'+interventionItem.name + '</i><br/><br/>' +
      'Wann ist das erlaubt?<br/>'+
      'Welche Konsequenzen hat das?<br/>'+
      'Müssten die Nutzer von der Löschung ihrer Daten unterrichtet werden?';
    this.help.confirmDelete(deleteText).then(() => {
      let result = false;
      switch (this.interventionInfo.type) {
        case InterventionType.ExamFeedback: 
          result = this.mwb.configuration.deleteExamFeedback(interventionItem.id);
          break;
        case InterventionType.TextAnalysis: 
          result = this.mwb.configuration.deleteWritingTask(interventionItem.id);
          break;
        default:
          this.help.popupInfo('invalid operation', 'delete '+ this.interventionInfo.type + ' not yet supported');
          return;
      }
      this.loadInterventionItems();
      this.help.popupDelete(result);
    })
    .finally(() => {});
  }
}
