import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WritingTaskEditComponent } from './writing-task-edit.component';

describe('WritingTaskEditComponent', () => {
  let component: WritingTaskEditComponent;
  let fixture: ComponentFixture<WritingTaskEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WritingTaskEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WritingTaskEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
