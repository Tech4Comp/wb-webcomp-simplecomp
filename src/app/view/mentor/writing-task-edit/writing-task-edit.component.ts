import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { configEditor } from 'src/app/configuration/editor.config';
import { MwbConfiguration } from 'src/app/configuration/mwb.config';
import { Intervention } from 'src/app/model/intervention';
import { Resource } from 'src/app/model/resource';
import { VarText } from 'src/app/model/varText';
import { WritingTask } from 'src/app/model/textAnalysis';
import { HelpComponent } from '../../utils/help/help.component';

@Component({
  selector: 'app-writing-task-edit',
  templateUrl: './writing-task-edit.component.html',
  styleUrls: ['./writing-task-edit.component.css']
})
export class WritingTaskEditComponent implements OnInit {
  @ViewChild('help') help: HelpComponent ; 
  public boxLabelText: string;
  public resourceId: string;
  public resourceInfo: Resource;
  public isValidResource: boolean;
  public interventionId: string;
  public interventionInfo: Intervention;
  public isValidIntervention: boolean;
  public writingTaskId: string;
  public writingTaskInfo: WritingTask;
  public editorConfig: AngularEditorConfig;

  constructor(
    private route: ActivatedRoute,
    private mwb: MwbConfiguration,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.writingTaskId = params['itemId'];
      this.writingTaskInfo = this.mwb.configuration.getWritingTaskInfo(this.writingTaskId);
      this.interventionId = this.writingTaskInfo.interventionId;
      this.interventionInfo = this.mwb.configuration.getInterventionInfo(this.interventionId);
      this.resourceId = this.interventionInfo.resourceId;
      this.resourceInfo = this.mwb.configuration.getResourceInfo(this.resourceId);
      this.isValidResource = this.mwb.configuration.isValidResource(this.resourceId);
      this.isValidIntervention = this.mwb.configuration.isValidIntervention(this.interventionId);
      this.editorConfig = configEditor();

      // todo: handle the properties task and compareToText (currently there is no interface for that)
      this.loadTask();
      this.loadCompareToText();
    });
  }

  private loadTask() {
    this.writingTaskInfo.task = new VarText();
  }

  private loadCompareToText() {
    this.writingTaskInfo.compareToText = '';
  }

  public showHelpForWritingTask() {
    this.help.popupHelp(['infoTextForWritingTask']);
  }

  public showHelpForCompare() {
    this.help.popupHelp(['infoTextForComparison']);
  }

  public save() {
    // todo: store values for task and compareToText (currently there is no interface for that)
    console.log('save task: ', this.writingTaskInfo.task);
    console.log('save compareToText: ', this.writingTaskInfo.compareToText);
    let result = this.mwb.configuration.saveWritingTask(this.writingTaskInfo);
    this.help.popupSave(result);
  }
}
