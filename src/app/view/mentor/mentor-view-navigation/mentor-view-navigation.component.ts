import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-mentor-view-navigation',
  templateUrl: './mentor-view-navigation.component.html',
  styleUrls: ['./mentor-view-navigation.component.css']
})
export class MentorViewNavigationComponent implements OnInit {
  @Input() resourceId: string;
  @Input() selected: string;
  public items_ = ['results', 'interventions', 'settings']
  public items = {
    results: 'Ergebnisse',
    interventions: 'Interventionen',
    settings: 'Einstellungen'
  }

  constructor() { }

  ngOnInit(): void {

  }

  public getNavigationItems() {
    let result = [];
    for (let item in this.items)
      result.push(item);
    return result;
  }

  public getText(item: string) {
    return this.items[item];
  }
}
