import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorViewNavigationComponent } from './mentor-view-navigation.component';

describe('MentorViewNavigationComponent', () => {
  let component: MentorViewNavigationComponent;
  let fixture: ComponentFixture<MentorViewNavigationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MentorViewNavigationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorViewNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
