import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { HelpComponent } from '../../utils/help/help.component';

@Component({
  selector: 'app-simple-bot-edit',
  templateUrl: './simple-bot-edit.component.html',
  styleUrls: ['./simple-bot-edit.component.css']
})
export class SimpleBotEditComponent implements OnInit {
  @Input() botName: string;
  @Output('botNameChange') changeBotNameEvent: EventEmitter<string>;
  @ViewChild('help') help: HelpComponent; 

  constructor() { 
    this.changeBotNameEvent = new EventEmitter<string>();
  }

  ngOnInit(): void {
  }

  public changeBotName() {
    this.changeBotNameEvent.emit(this.botName);
  }

  public showBotInfo() {
    this.help.popupInfo('Tabelle Bot','Diese Angabe wird aktuell noch nicht gespeichert.<br/>Wird das ein Dropdown, bei dem man aus den vorhandenen Bots auswählen kann?', 'Info');
  }
}
