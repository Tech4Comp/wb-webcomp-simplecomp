import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleBotEditComponent } from './simple-bot-edit.component';

describe('SimpleBotEditComponent', () => {
  let component: SimpleBotEditComponent;
  let fixture: ComponentFixture<SimpleBotEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SimpleBotEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleBotEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
