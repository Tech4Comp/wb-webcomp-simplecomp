import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MwbConfiguration } from 'src/app/configuration/mwb.config';
import { Resource } from 'src/app/model/resource';
import { UserInfo } from 'src/app/model/userInfo';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { configEditor } from 'src/app/configuration/editor.config';
import { HelpComponent } from '../../utils/help/help.component';
import { forceName } from '../../utils/utils';
import { Router } from '@angular/router';

@Component({
  selector: 'app-resource-edit',
  templateUrl: './resource-edit.component.html',
  styleUrls: ['./resource-edit.component.css']
})
export class ResourceEditComponent implements OnInit {
  @ViewChild('help') help: HelpComponent ; 
  public selected = "settings";
  public resourceId: string;
  public resourceInfo: Resource;
  public isValidResource: boolean;
  public editorConfig: AngularEditorConfig;

  constructor(
    private mwb: MwbConfiguration,
    private userInfo: UserInfo,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.resourceId = this.userInfo.resourceId;
    this.resourceInfo = this.mwb.configuration.getResourceInfo(this.resourceId);
    this.isValidResource = this.mwb.configuration.isValidResource(this.resourceId);
    this.editorConfig = configEditor();
    this.initTexts();
  }

  private initTexts() {
  }

  public preview() {
    let title = 'Vorschau';
    let body = 'wie soll die aussehen?';
    this.help.popupInfo(title, body);
  }

  public save() {
    let result = this.mwb.configuration.saveResource(this.resourceInfo);
    this.help.popupSave(result);
  }

  public delete() {
    let deleteText = 'Lösche '+' <i>'+forceName(this.resourceInfo.name) + '</i><br/><br/>' +
      'Wann ist das erlaubt?<br/>'+
      'Welche Konsequenzen hat das?<br/>'+
      'Müssten die Nutzer von der Löschung ihrer Daten unterrichtet werden?';
    this.help.confirmDelete(deleteText).then(() => {
      let result = this.mwb.configuration.deleteResource(this.resourceId);
      this.help.popupDelete(result);
      if (result)
        this.router.navigate(['/overview/']);
    })
    .finally(() => {});
  }

}
