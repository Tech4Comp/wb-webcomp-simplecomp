import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamFeedbackEditComponent } from './exam-feedback-edit.component';

describe('ExamFeedbackEditComponent', () => {
  let component: ExamFeedbackEditComponent;
  let fixture: ComponentFixture<ExamFeedbackEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExamFeedbackEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamFeedbackEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
