import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { MwbConfiguration } from 'src/app/configuration/mwb.config';
import { GetInterventionItemsName, Intervention } from 'src/app/model/intervention';
import { Resource } from 'src/app/model/resource';
import { ExamFeedback } from 'src/app/model/examFeedback';
import { VarText } from 'src/app/model/varText';
import { HelpComponent } from 'src/app/view/utils/help/help.component';


@Component({
  selector: 'app-exam-feedback-edit',
  templateUrl: './exam-feedback-edit.component.html',
  styleUrls: ['./exam-feedback-edit.component.css']
})
export class ExamFeedbackEditComponent implements OnInit {
  @ViewChild('help') help: HelpComponent ; 
  public resourceId: string;
  public resourceInfo: Resource;
  public isValidResource: boolean;
  public interventionId: string;
  public interventionInfo: Intervention;
  public isValidIntervention: boolean;
  public examFeedbackId: string;
  public examFeedbackInfo: ExamFeedback;
  public editorConfig: AngularEditorConfig;
  public pageCnt = 3;
  public page = 1;
  
  constructor(
    private route: ActivatedRoute,
    private mwb: MwbConfiguration,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.examFeedbackId = params['itemId'];
      this.examFeedbackInfo = this.mwb.configuration.getExamFeedbackInfo(this.examFeedbackId);
      this.interventionId = this.examFeedbackInfo.interventionId;
      this.interventionInfo = this.mwb.configuration.getInterventionInfo(this.interventionId);
      this.resourceId = this.interventionInfo.resourceId;
      this.resourceInfo = this.mwb.configuration.getResourceInfo(this.resourceId);
      this.isValidResource = this.mwb.configuration.isValidResource(this.resourceId);
      this.isValidIntervention = this.mwb.configuration.isValidIntervention(this.interventionId);
      
      // todo: handle the properties task and compareToText (currently there is no interface for that)
      this.loadInfoText();
    });
  }
  private loadInfoText() {
    this.examFeedbackInfo.infoText = new VarText();
  }

  public getPageText(page) {
    return page+'. '+['Allgemeine Infos', 'Test verknüpfen','Metadaten verknüpfen'][page-1];
  }

  public showHelpForInfoText() {
    this.help.popupHelp(['infoTextForExamFeedback']);
  }

  public interventionItemsName() {
    return GetInterventionItemsName(this.interventionInfo.type);
  }

  public back() {
    if (this.page > 0)
      this.page--;
  }

  public next() {
    if (this.page < this.pageCnt)
      this.page++;
  }

  public save() {
    // todo: store values for task and compareToText (currently there is no interface for that)
    console.log('save task: ', this.examFeedbackInfo.infoText);

    let result = this.mwb.configuration.saveExamFeedback(this.examFeedbackInfo);
    this.help.popupSave(result);
  }
}
