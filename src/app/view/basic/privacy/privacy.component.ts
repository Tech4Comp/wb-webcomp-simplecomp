import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login-service';
import { Resource } from 'src/app/model/resource';
import { redirectToResourceAndIntervention } from 'src/app/app-routing/guard';
import { Router } from '@angular/router';
import { PrivacyService } from 'src/app/services/privacy-service';
import { UserInfo } from 'src/app/model/userInfo';
import { WorkbenchService } from 'src/app/services/workbench.service';
import { MwbConfiguration } from 'src/app/configuration/mwb.config';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent implements OnInit {
  public resourceInfo: Resource;
  public hasAgreed = false;
  public hasRevoked = false;
  public requestsRevoke = false;

  constructor(
    private loginService: LoginService,
    private privacyService: PrivacyService,
    private userInfo : UserInfo,
    private router: Router,
    private mwb: MwbConfiguration,
    private mwbService: WorkbenchService
  ) { } 

  ngOnInit(): void {
    if (!this.privacyService.needsPrivacyStatement())
      this.redirect();
    else {
      this.resourceInfo = this.mwb.configuration.getResourceInfo(this.userInfo.resourceId);
      this.privacyService.deleteDeprecatedCookieVersions();
      this.hasAgreed = this.privacyService.hasAgreedPrivacyStatement();
      this.requestsRevoke = this.hasAgreed;
    }
  }

  public showPrivacyStatement() {
    return !this.hasAgreed && !this.requestsRevoke && !this.hasRevoked;
  }

  public agree() {
    this.privacyService.agreePrivacyStatement();
    this.hasAgreed = true;
    this.redirect();
  }

  public disagree() {
    this.hasAgreed = false;
    this.hasRevoked = true;
  }


  public showConfirmStatement() {
    return this.requestsRevoke;
  }

  public revoke() {
    this.privacyService.removeAgreement();
    this.requestsRevoke = false;
    this.hasAgreed = false;
    this.hasRevoked = true;
    this.deleteUserdata();
  }

  public cancelRevoke() {
    this.requestsRevoke = false;
    this.hasAgreed = false;
    this.hasRevoked = false;
  }


  public showDisagreeStatement() {
   return this.hasRevoked && !this.requestsRevoke;
  }


  async deleteUserdata() {
    // not yet used: users have to send an email to get their data deleted
    return;

    // only delete data for resourceId?
    // if no resourceId: delete all data, revoke all cookies and logout and remove lrs-account?
    this.loginService.loadUser()
      .then(user => {
        this.mwbService.deleteUserdata().subscribe((data: any[])=>{
          console.log(data);
          this.loginService.logout()
        })
      })
  }


  private redirect() {
    redirectToResourceAndIntervention(this.router, this.userInfo.resourceId, this.userInfo.interventionId);
  }
}
