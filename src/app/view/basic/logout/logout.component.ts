import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login-service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(
    private loginService: LoginService
  ) { }

  ngOnInit(): void {
  }

  public onRocketChatLogout() {
    this.loginService.logout();
  }
}
