import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { WorkbenchService } from "src/app/services/workbench.service";
import { IntentDistribution } from "src/app/model/intentDistribution";
import { UserInfo } from 'src/app/model/userInfo';
import { LoginService } from 'src/app/services/login-service';
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { IntentPath, PathNode, PathEdge } from "src/app/model/intentPath";
import { DataSet } from 'vis-data/peer';
import { Network } from 'vis-network/peer';

const cChooseBot = "Bot auswählen";
const cChooseAnalysis = "Analyseart auswählen";
const cChooseIntentDistributionDistinct = "Intentverteilung (nur verschiedene Nachrichten)";
const cChooseIntentDistributionAll = "Intentverteilung (alle Nachrichten)";
const cChoosePathAnalysis = "Genutzte Pfade";
const cChooseNone = "Keine auwählen";

@Component({
  selector: 'app-bot-analysis',
  templateUrl: './bot-analysis.component.html',
  styleUrls: ['./bot-analysis.component.css',
                '../../../.././node_modules/bootstrap/dist/css/bootstrap.min.css']
})
export class BotAnalysisComponent implements OnInit {

  availableBots : String[];
  chosenBot: String = cChooseBot;
  chosenSemester: String = "Sommersemester 2022";
  chosenSemesterCode = "ss22";
  chosenAnalysis: String = "";
  chosenAnalysisButton: String = cChooseAnalysis;
  botChoiceNotCorrect = false;
  analysisChoiceNotCorrect = false;
  myCChooseNone = cChooseNone;
  myCChooseIntentDistributionDistinct = cChooseIntentDistributionDistinct;
  myCChooseIntentDistributionAll = cChooseIntentDistributionAll;
  myCChoosePathAnalysis = cChoosePathAnalysis;

  public pieChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false
  };
  public pieChartLabels: Label[] = [];
  public pieChartData: SingleDataSet = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  isLoading:boolean = false;
  hasLoaded:boolean = false;
  noPathData:boolean = false;

  modelFrom: NgbDateStruct = { year : 2020, month : 1, day : 1 };
  modelTo: NgbDateStruct = this.calendar.getToday();
  modelToday: NgbDateStruct = this.calendar.getToday();
  date: {year: number, month: number, day: number};
  date2: {year: number, month: number, day: number};

  //intentData: IntentDistribution[];

  private visNetwork: ElementRef;
//  @ViewChild('visNetwork', { static: false }) visNetwork!: ElementRef;
  @ViewChild('visNetwork', { static: false }) set content(content: ElementRef) {
    if (content) {
      this.visNetwork = content;
    }
  }


  private networkInstance: any;
  pathData: IntentPath;

  constructor( private userInfo: UserInfo,
               private workbenchService: WorkbenchService,
               private loginService: LoginService,
               private calendar: NgbCalendar
               ) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  ngOnInit(): void {
    this.availableBots = ['bitbot', 'chemnitzbot', 'BiWi-LitBot', 'BiWi-FeedBot']

/*        this.intentData = [ {
            "name" : "creditstatus",
            "id" : "creditstatus",
            "value" : 820
          }, {
          ...
          } ];
          */

  }

  public chooseBot(bot: String) {
    if (bot != "")
      this.chosenBot = bot;
    else
      this.chosenBot = cChooseBot;
  }

  public chooseSemester(semester: String) {
    this.chosenSemester = semester;
  }

  public chooseAnalysis(analysis: String) {

    if (this.chosenAnalysis != analysis) {
      this.hasLoaded = false;
      this.isLoading = false;
      this.noPathData = false;
      this.visNetwork = null;
    }

    if (analysis == "IntentverteilungDistinct") {
      this.chosenAnalysis = analysis;
      this.chosenAnalysisButton = cChooseIntentDistributionDistinct;
    }
    else if (analysis == "IntentverteilungAll") {
      this.chosenAnalysis = analysis;
      this.chosenAnalysisButton = cChooseIntentDistributionAll;
    }
    else if (analysis != "")
    {
      this.chosenAnalysis = analysis;
      this.chosenAnalysisButton = analysis;
    }
    else {
      this.chosenAnalysis = "";
      this.chosenAnalysisButton = cChooseAnalysis;
    }
  }

  public getChart() {
    console.log("getting");
    console.log(this.chosenAnalysis);
    this.noPathData = false;

    //Format: "2021-09-01T23:59:59.000Z";
    //var time1 = this.modelFrom.year + "-" + this.modelFrom.month + "-" + this.modelFrom.day + "T00:00:00.000Z";
    //var time2 = this.modelTo.year + "-" + this.modelTo.month + "-" + this.modelTo.day + "T23:59:59.999Z";

    if (this.chosenAnalysis == "")
    {
      this.analysisChoiceNotCorrect = true;
      this.botChoiceNotCorrect = false;
    }
    else if (this.chosenBot == cChooseBot)
    {
      this.botChoiceNotCorrect = true;
      this.analysisChoiceNotCorrect = false;
    }
    else
    {
      this.botChoiceNotCorrect = false;
      this.analysisChoiceNotCorrect = false;
      this.isLoading = true;
      this.hasLoaded = false;

      if (this.chosenSemester == "Wintersemester 2021/22")
        this.chosenSemesterCode = "ws22";
      else
        this.chosenSemesterCode = "ss22";

      if (this.chosenAnalysis.startsWith("Intentverteilung"))
      {
        this.pieChartData = [];
        this.pieChartLabels = [];
        this.workbenchService.getBotIntentDistribution(this.chosenBot, this.chosenAnalysis, this.chosenSemesterCode).subscribe((data: IntentDistribution[]) => {
          if (this.loginService.hasValidIdToken()) {
          console.log(data);
          if (data.length == 0)
            this.noPathData = true;
          else
            for (var i = 0; i < data.length; i++)
            {
              this.pieChartData.push(data[i].value);
              this.pieChartLabels.push(data[i].name);
            }
            this.hasLoaded = true;
          }
        })
      }
      else if (this.chosenAnalysis == cChoosePathAnalysis)
      {
  //    this.workbenchService.getIntentPath(this.chosenBot, time1, time2).subscribe((data: IntentDistribution[]) => {
        this.workbenchService.getIntentPath(this.chosenBot, this.chosenSemesterCode).subscribe((data: IntentPath) => {
          if (this.loginService.hasValidIdToken()) {
            console.log(data);
            this.pathData = data;

            if (this.pathData.nodes.length == 0) {
              this.noPathData = true;
              this.hasLoaded = true;
            }
            else {

              for (let i in this.pathData.edges) {
                  const edge = this.pathData.edges[i];
                  if (edge.value <= 10) {
                      edge.value = 1;
                      edge.color = {color: '#E8E8E8'};
                      edge.dashes = [2,2];
                  }else{
                      edge.color = {color: '3390FF'};
                  }
              }
              for (let i in this.pathData.nodes) {
                  const node = this.pathData.nodes[i];
                  if (node.type === 'recognizedIntent') {
                      node.color = "#FFA807"
                  }
                  else if(node.type === 'ignoredText'){
                      node.color = "#D3D3D3"
                  }
                  else{
                      node.color = "#3390FF"
                  }
              }

              const nodes = new DataSet<any>(this.pathData.nodes);
              const edges = new DataSet<any>(this.pathData.edges);
              const path = { nodes, edges };

              this.networkInstance = new Network(this.visNetwork.nativeElement, path, {
                physics: {enabled: false},
                nodes: {
                    shape: "dot"
                },
                edges: {
                    arrows: {
                        to: {
                            enabled: true,
                            scaleFactor: 0.1
                        }

                    }
                },
                height: '500px',
                width: '800px',
              });
              this.networkInstance.stabilize(2000);
            }
          }
        })

      /*  this.pathData = {
          "nodes" : [ {
            "label" : "multi_sentence",
       ...
            "value" : 8
          } ]
        }
        */

      }
    }
  }

  public async getLogs() {
    for (var i = 0; i < this.availableBots.length; i++) {
      let bot = this.availableBots[i];
      let semester = "ss22";
      console.log("Semester: " + semester);
      let times = [];
      //if (bot == "BiWi-FeedBot")
      //  times = ["2022-05-01T00:00:01.000Z", "2022-06-01T00:00:01.000Z", "2022-07-01T00:00:01.000Z", "2022-08-01T00:00:01.000Z", "2022-09-01T00:00:01.000Z", "2022-10-01T00:00:01.000Z"];
      //else
        times = ["2022-10-01T00:00:01.000Z"];
      for (var j = 0; j < times.length; j++) {
        let upto = times[j];
        let answer;
        var response = await this.workbenchService.getBotLogs(bot, semester, upto).toPromise();
        if (response)
          console.log(response + " up to " + upto);
      }
      semester = "ws22";
      console.log("Semester: " + semester);
      times = [];
      //if (bot == "BiWi-FeedBot")
//        times = ["2021-10-10T00:00:01.000Z", "2021-10-20T00:00:01.000Z", "2021-11-01T00:00:01.000Z", "2021-11-10T00:00:01.000Z", "2021-11-20T00:00:01.000Z", "2021-12-01T00:00:01.000Z", "2021-12-10T00:00:01.000Z", "2021-12-20T00:00:01.000Z", "2022-01-01T00:00:01.000Z", "2022-01-11T00:00:01.000Z", "2022-01-21T00:00:01.000Z", "2022-02-01T00:00:01.000Z", "2022-02-11T00:00:01.000Z", "2022-02-21T00:00:01.000Z", "2022-03-01T00:00:01.000Z", "2022-03-11T00:00:01.000Z", "2022-03-21T00:00:01.000Z", "2022-04-01T00:00:01.000Z"];
      //  times = ["2021-11-01T00:00:01.000Z", "2021-12-01T00:00:01.000Z", "2022-01-01T00:00:01.000Z", "2022-02-01T00:00:01.000Z", "2022-03-01T00:00:01.000Z", "2022-04-01T00:00:01.000Z"];
      //else
      //  times = ["2022-01-01T00:00:01.000Z", "2022-04-01T00:00:01.000Z"]
        times = ["2022-04-01T00:00:01.000Z"]
        for (var j = 0; j < times.length; j++) {
        let upto = times[j];
        let answer;
        var response = await this.workbenchService.getBotLogs(bot, semester, upto).toPromise();
        if (response)
          console.log(response + " up to " + upto);
      }

    }
  }

}
