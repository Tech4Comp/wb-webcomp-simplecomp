import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LrsAnalyticsComponent } from './lrs-analytics.component';

describe('LrsAnalyticsComponent', () => {
  let component: LrsAnalyticsComponent;
  let fixture: ComponentFixture<LrsAnalyticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LrsAnalyticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LrsAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
