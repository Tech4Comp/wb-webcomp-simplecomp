import { Component, Input, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { interval, Subscription } from 'rxjs';
import { Intervention } from 'src/app/model/intervention';
import { SmallRecord } from "../../../model/record";
import { WorkbenchService } from "../../../services/workbench.service";

@Component({
  selector: 'intervention-knowledge-graph',
  templateUrl: './knowledge-graph.component.html',
  styleUrls: ['./knowledge-graph.component.css']
})

export class KnowledgeGraphComponent implements OnInit {
  @Input() interventionInfo: Intervention;
  @ViewChild('iframe') iframe: ElementRef;
  

  private serviceGraphToolUrl = 'https://thor.easlit.erzw.uni-leipzig.de';
  private serviceMessageTypes = {
    userChosePersonalGraph: 'userChosePersonalGraph',
    newSession: 'newSession'
  }  

  iframeUrl: SafeResourceUrl = '';
  private readonly urlOptions = '/sigma?lrs=true&debug=true';
  private actions: string[] = [];
  private interval$: Subscription;
  private periodForceWriteAfterInactivity = 60*1000;
  private isInitialised: boolean = false;

  // @HostListener('window:beforeunload', ['$event'])
  // onBeforeUnload() {
  //   window.history.replaceState(null, null, window.location.href + "/intervention/" + this.interventionInfo.id);
  //   console.log('host listener before unload');
  //   this.logActionsToLRS();
  //   return false;
  // //   if (this.hasUnloggedActions())
  // //     return true;
  // //   window.history.replaceState(null, null, window.location.href + "/intervention/" + this.interventionInfo.id);
  // //   this.logActionsToLRS();
  // //   return false;
  // }

  // @HostListener('window:unload', ['$event'])
  // onUnload() {
  //   return false;
  // }


  constructor(
    private workbenchService: WorkbenchService,
    private sanitizer: DomSanitizer
  ) {
    window.addEventListener('message', (event) => {
      if (this.isMessageFromGraphTool(event.origin))
        this.processDataFromGraphTool(event.data);
      else 
        this.handleUnspecifiedMessage(event.origin);
    })
  }

  ngOnInit(): void {
    this.iframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.serviceGraphToolUrl+this.urlOptions);
  }

  ngAfterViewInit() {
    console.log('after view init');
    this.isInitialised = true;
  }

  onIframeLoad() {
    console.log('iframe loaded');
    if (this.isInitialised) {
      this.getGraphListAndSendToGraphTool();
    }
    // console.log('test log KgUse');
    // this.workbenchService.logKgUse(this.interventionInfo.id, []).subscribe((data) => console.log('logged', data));
  }


  ngOnDestroy(): void {
    console.log('destroy');
    if (this.interval$)
      this.interval$.unsubscribe();
    this.logActionsToLRS();
  }


  private isMessageFromGraphTool(origin) {
    return origin === this.serviceGraphToolUrl;
  }

  private processDataFromGraphTool(record) {
    switch (record.action) {
      case this.serviceMessageTypes.userChosePersonalGraph:
        this.getJsonGraphForRecordAndSendToGraphTool(record.data);
        break;
      case this.serviceMessageTypes.newSession:
        console.log('newSession');
        this.logActionsToLRS();
        break;
    }
    this.addToActions(record);
  }

  private handleUnspecifiedMessage(origin) {
    console.log('ignore message from', origin);
  }

  private getGraphListAndSendToGraphTool() {
    this.workbenchService.getRecordsForGraphTool(this.interventionInfo.id).subscribe((recordList: SmallRecord[]) => {
      this.sendRecordListToGraphTool(recordList);
    })
  }

  public sendRecordListToGraphTool(recordList: SmallRecord[]) {
    console.log("send records to graphTool via iframe");
    let record = { kind : "graphList", data : recordList };
    this.iframe.nativeElement.contentWindow.postMessage(record, this.serviceGraphToolUrl);
  }

  private getJsonGraphForRecordAndSendToGraphTool(statementID) {
    console.log("get json-graph for single record and send it to graphTool via iframe");
    this.workbenchService.getJsonGraphForRecord(this.interventionInfo.id, statementID).subscribe((jsonData: string) => {
      let jsonGraph = { kind : "graphData", data : JSON.parse(jsonData) };
      this.iframe.nativeElement.contentWindow.postMessage(jsonGraph, "*");
    })
  }

  private hasUnloggedActions() : boolean {
    return this.actions.length > 0;
  }

  private addToActions(record) {
    this.actions.push(record);
    this.interval$ = interval(this.periodForceWriteAfterInactivity).subscribe(() => {
      console.log('time elapsed');
      this.logActionsToLRS();
    });
  }

  private logActionsToLRS() {
    console.log('logActionsToLRS');
    if (this.hasUnloggedActions()) {
      console.log('log actions', this.actions);
      this.workbenchService.logKgUse(this.interventionInfo.id, this.actions).subscribe(() => {});
      this.actions = [];
    }
  }
}
