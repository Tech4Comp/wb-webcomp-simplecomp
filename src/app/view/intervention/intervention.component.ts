import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Intervention, InterventionType } from 'src/app/model/intervention';
import { Resource } from "../../model/resource";
import { UserInfo } from 'src/app/model/userInfo';
import { MwbConfiguration } from 'src/app/configuration/mwb.config';

@Component({
  templateUrl: './intervention.component.html',
  styleUrls: ['./intervention.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class InterventionComponent implements OnInit {
  resourceId: string;
  isValidResource: boolean;
  isValidIntervention: boolean;
  interventionId: string;
  interventionInfo: Intervention;
  interventionView: string = "";
  resourceInfo: Resource;
  isInterventionError: boolean = false;
  type: InterventionType = InterventionType.None;
  typeEnum: any; // propagate enum to html

  constructor(
    private userInfo: UserInfo, 
    private mwb: MwbConfiguration
  ) { }

  ngOnInit(): void {
    this.typeEnum = InterventionType;
    this.interventionId = this.userInfo.interventionId;
    this.resourceId = this.mwb.configuration.getResourceIdForIntervention(this.interventionId);
    this.interventionInfo = this.mwb.configuration.getInterventionInfo(this.interventionId);
    this.resourceId = this.interventionInfo.resourceId;
    this.resourceInfo = this.mwb.configuration.getResourceInfo(this.resourceId);
    this.isValidResource = this.mwb.configuration.isValidResource(this.resourceId);
    this.isValidIntervention = this.mwb.configuration.isValidIntervention(this.interventionId);
   }
}
