import { Component, ElementRef, HostListener, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'intervention-rocket-chat',
  inputs: ['botName', 'interventionId', 'forLogoutOnly'],
  outputs: ['logoutEvent: loggedOut'],
  templateUrl: './rocket-chat.component.html',
  styleUrls: ['./rocket-chat.component.css']
})
export class RocketChatComponent implements OnInit {
  botName: string;
  interventionId: string;
  forLogoutOnly: string;
  logoutEvent: EventEmitter<string>;

  @HostListener('window:message', ['$event'])
  onMessage(event: MessageEvent) {
    if (event.origin != this.chatBaseUrl)
      return;
    switch (event.data.eventName) {
      case 'Custom_Script_Logged_Out':
        this.rocketChatMessageLoggedOut();
        break;
      case 'Custom_Script_Logged_In':
        this.rocketChatMessageLoggedIn();
        break;
    }
  }

  @ViewChild('mwbIframeRocketChat', { static: false, read: ElementRef })
  private mwbIframeRocketChat: ElementRef;
  private rocketChatLoggedIn = false;
  private chatBaseUrl = 'https://chat.tech4comp.dbis.rwth-aachen.de';
  public chatUrl: SafeResourceUrl = '';
  readonly embedded = '?layout=embedded';

  constructor(
    private sanitizer: DomSanitizer
  ) {
    this.logoutEvent = new EventEmitter<string>();
  }

  ngOnInit(): void {
    this.chatUrl = this.initChatUrl()
  }

  private initChatUrl() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.chatBaseUrl+ '/home/'+this.embedded);
  }

  private postMessageToRocketChat(message : any) {
    this.mwbIframeRocketChat.nativeElement.contentWindow.postMessage(message, '*');
  }

  private rocketChatMessageLoggedOut() {
    this.rocketChatLoggedIn = false;
    if (this.forLogoutOnly) {
      this.logoutEvent.emit('logout');
    }
  }

  private rocketChatMessageLoggedIn() {
    if (this.forLogoutOnly) {
      this.logout();
      return;
    }
    if (!this.rocketChatLoggedIn)
      this.goToBot();
    this.rocketChatLoggedIn = true;
  }

  public showLogoutButton() {
    let showButton = false;
    return showButton && this.rocketChatLoggedIn;
  }

  public btnLogout() {
    this.logout();
  }

  public btnLogin() {
    this.login();
  }

  public btnGoHome() {
    this.goHome();
  }

  public btnGoToBot() {
    this.goToBot();
  }

  private goHome() {
    this.postMessageToRocketChat({
      externalCommand: 'go',
      path: '/home'+this.embedded
    });
  }

  private goToBot() {
    if (this.botName) {
      this.postMessageToRocketChat({
        externalCommand: 'go',
        path: '/direct/'+this.botName+this.embedded
      });
    }
  }

  private logout() {
    this.postMessageToRocketChat({
      externalCommand: 'logout'
    });
  }

  private login() {
    // no possibility to send an authToken, this command will only trigger the login popup?
    // but (due to current rocketchat settings?) this one is not working at all
    this.postMessageToRocketChat({
      externalCommand: 'call-custom-oauth-login',
      service: 'Learninglayers'
    });
  }

}
