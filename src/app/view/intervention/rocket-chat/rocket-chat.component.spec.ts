import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RocketChatComponent } from './rocket-chat.component';

describe('RocketChatComponent', () => {
  let component: RocketChatComponent;
  let fixture: ComponentFixture<RocketChatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RocketChatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RocketChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
