import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleBotComponent } from './simple-bot.component';

describe('SimpleBotComponent', () => {
  let component: SimpleBotComponent;
  let fixture: ComponentFixture<SimpleBotComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SimpleBotComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleBotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
