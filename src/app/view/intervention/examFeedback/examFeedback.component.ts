import { Component, OnInit, Input } from '@angular/core';
import { DatePipe } from '@angular/common';
import { WorkbenchService } from "src/app/services/workbench.service";
import { Intervention } from "src/app/model/intervention";
import { MwbConfiguration } from 'src/app/configuration/mwb.config';
import { ExamFeedback } from 'src/app/model/examFeedback';
import { ThrowStmt } from '@angular/compiler';

const datePipe = new DatePipe('de');

@Component({
  selector: 'intervention-exam-feedback',
  templateUrl: './examFeedback.component.html',
  styleUrls: ['./examFeedback.component.css']
})
export class ExamFeedbackComponent implements OnInit {

  @Input() interventionInfo: Intervention;

  examFeedbackInfo: ExamFeedback;
  examIsOver: boolean = false;
  hasExamFeedback: boolean = false;
  contentBeforeExam: string = '';
  contentAfterExam: string = '';

  constructor(
    private mwb: MwbConfiguration,
    private workbenchService: WorkbenchService
  ) { }

  ngOnInit(): void {
    // console.log((new Date('2022-12-19')).getTime());
    this.examFeedbackInfo = this.mwb.configuration.getExamFeedbackInfo(this.interventionInfo.id);
    this.examIsOver = this.examFeedbackInfo.visibleFrom <= Date.now();
    this.getVariableContent();
    this.workbenchService.hasExamFeedback(this.interventionInfo.id)
      .subscribe((data: string) => {
        console.log("hasexamfeedback: " + data);
        if (data == "true")
          this.hasExamFeedback = true;
        else {
          this.workbenchService.hasTestResult(this.interventionInfo.id).subscribe((data2: string)=>{
            console.log("hastestresult: " + data2);
            if (data2 == "true") {
               this.workbenchService.createExamFeedback(this.interventionInfo.id).subscribe((data3: string) => {
                this.workbenchService.hasExamFeedback(this.interventionInfo.id).subscribe((data4: string)=>{
                  console.log("hasexamfeedback: " + data4);
                  if (data4 == "true")
                    this.hasExamFeedback = true;
                })
               });
            }
          })
        }
      //this.getExamFeedback();
      /*  this.workbenchService.getUserNotifications().subscribe((data: any[])=>{
          this.userNotifications = data;
          console.log("Notifications: " + this.userNotifications);
        });   */
      });
  }

 /* private getExamFeedback() {
    this.workbenchService.getTestFeedback(this.interventionInfo.id).subscribe((data: string) => {
      this.testFeedbackFile = 'data:application/pdf;base64,' + data;
    })
  }*/

  public getVariableContent() {
    this.setIntroText();
    if (this.examIsOver)
      this.setContentAfterExam();
    else
      this.setContentBeforeExam();
  }

  private setContentBeforeExam() {
    this.contentBeforeExam = this.replaceWildcardStrings(this.examFeedbackInfo.contentBeforeExam);
  }

  private setContentAfterExam() {
    this.contentAfterExam = this.replaceWildcardStrings(this.examFeedbackInfo.contentAfterExam);
  }

  private setIntroText() {
    this.interventionInfo.introText = this.replaceWildcardStrings(this.interventionInfo.introText);
  }

  private replaceWildcardStrings(content: string) : string {
    let displayDateFrom = datePipe.transform(this.examFeedbackInfo.visibleFrom, 'dd. MMMM yyyy');
    let displayDateTo = datePipe.transform(this.examFeedbackInfo.visibleFrom, 'dd. MMMM yyyy');
    return content.replace(new RegExp(/{{date}}/gi), displayDateFrom)
      .replace(new RegExp(/{{dateFrom}}/gi), displayDateFrom)
      .replace(new RegExp(/{{dateTo}}/gi), displayDateTo)
      .replace(new RegExp(/{{examName}}/gi), this.examFeedbackInfo.name);
  }

  private openBlobForIE(newBlob) : boolean {
    // IE doesn't allow using a blob object directly as link href
    // instead it is necessary to use msSaveOrOpenBlob
    let useIE = false;
    if (window.navigator) {
      const nav = (window.navigator as any);
      if (nav.msSaveOrOpenBlob) {
        useIE = true;
        nav.msSaveOrOpenBlob(newBlob);
      }
    }
    return useIE;
  }


  public downloadFeedbackFile(): void {
    this.workbenchService.getTestFeedback(this.interventionInfo.id)
      .subscribe(x => {
        // It is necessary to create a new blob object with mime-type explicitly set
        // otherwise only Chrome works like it should
        var newBlob = new Blob([x], { type: "application/pdf" });

        if (this.openBlobForIE(newBlob)) {
          return;
        }

        // For other browsers:
        // Create a link pointing to the ObjectURL containing the blob.
        const data = window.URL.createObjectURL(newBlob);

        var link = document.createElement('a');
        link.href = data;
        link.download = "Feedback-Probeklausur.pdf";
        // this is necessary as link.click() does not work on the latest firefox
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

        setTimeout(function () {
          // For Firefox it is necessary to delay revoking the ObjectURL
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      });
  }
}
