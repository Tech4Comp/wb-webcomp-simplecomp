import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamFeedbackComponent } from './examFeedback.component';

describe('ExamComponent', () => {
  let component: ExamFeedbackComponent;
  let fixture: ComponentFixture<ExamFeedbackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExamFeedbackComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
