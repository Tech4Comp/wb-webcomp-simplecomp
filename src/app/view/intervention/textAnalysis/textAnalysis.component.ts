import { Component, OnInit, Input } from '@angular/core';
import { MwbConfiguration } from 'src/app/configuration/mwb.config';
import { BotInfo } from 'src/app/model/botInfo';
import { Intervention } from "src/app/model/intervention";


@Component({
  selector: 'intervention-text-analysis',
  templateUrl: './textAnalysis.component.html',
  styleUrls: ['./textAnalysis.component.css']
})
export class TextAnalysisComponent implements OnInit {
  @Input() interventionInfo: Intervention;

    
  botInfo: BotInfo;
  botNameRocketChat: string;

  constructor(
    private mwb: MwbConfiguration,
  ) { }

  ngOnInit(): void {
    this.botInfo = this.mwb.configuration.getBotInfo(this.interventionInfo.id)
    this.botNameRocketChat = this.mwb.configuration.getBotNameForRocketChat(this.interventionInfo.id);
  }
 }
