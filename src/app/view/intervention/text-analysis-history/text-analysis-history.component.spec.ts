import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextAnalysisHistoryComponent } from './text-analysis-history.component';

describe('TextAnalysisHistoryComponent', () => {
  let component: TextAnalysisHistoryComponent;
  let fixture: ComponentFixture<TextAnalysisHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextAnalysisHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextAnalysisHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
