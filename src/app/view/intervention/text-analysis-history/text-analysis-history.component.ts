import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { firstValueFrom, interval, Subscription } from 'rxjs';
import { MwbConfiguration } from 'src/app/configuration/mwb.config';
import { Intervention } from 'src/app/model/intervention';
import { extractSmallRecord, Record, SmallRecord } from "src/app/model/record";
import { TextAnalysis } from 'src/app/model/textAnalysis';
import { WorkbenchService } from 'src/app/services/workbench.service';
import { KnowledgeGraphComponent } from '../knowledge-graph/knowledge-graph.component';

const datePipe = new DatePipe('de');

@Component({
  selector: 'intervention-text-analysis-history',
  templateUrl: './text-analysis-history.component.html',
  styleUrls: ['./text-analysis-history.component.css']
})
export class TextAnalysisHistoryComponent implements OnInit {
  @Input() interventionInfo: Intervention;
  @Input() knowledgeGraph: KnowledgeGraphComponent;

  serviceAvailable: boolean = true;
  textAnalysisInfo: TextAnalysis;
  nrOfTasks: number;
  interval$: Subscription;
  periodCheckForNews: number = 10*1000;
  lastChanged: number = 0;
  recordList: Record[] = [];
  indicesSortedByNumberDesc: number[] = [];
  indicesSortedByDate: number[] = [];
  sortedIndices: number[];
  isSortedByNumber: boolean = false;
  
  constructor(
    private workbenchService: WorkbenchService,
    private mwb: MwbConfiguration
  ) { }

  ngOnInit(): void {
    this.textAnalysisInfo = this.mwb.configuration.getTextAnalysisInfo(this.interventionInfo.id);
    this.nrOfTasks = this.mwb.configuration.getNumberOfWritingTasks(this.interventionInfo.id);
    this.getRecords();
    // this.workbenchService.getUserNotifications().subscribe((data: any[])=>{
    //     this.userNotifications = data;
    //     console.log("Notifications: " + this.userNotifications);
    //   });

  }

  ngOnDestroy(): void {
    if (this.interval$)
      this.interval$.unsubscribe();
  }

  private getRecords() {
    this.getRecordsFromService().then(() => {
      this.interval$ = interval(this.periodCheckForNews).subscribe(() => {
        this.checkForNews();
      });
    });
  }

  private checkForNews() {
    console.log('checkForNews', this.serviceAvailable);
    if (!this.serviceAvailable) {
      this.interval$.unsubscribe();
      return;
    }
    try {
      this.workbenchService.getLastEntry(this.interventionInfo.id).subscribe({
        next: lastEntry => {
          let hasNewRecords = Number(new Date(lastEntry)) > this.lastChanged;
          if (hasNewRecords) {
            console.log("NEWS NEWS NEWS NEWS NEWS");
            this.getRecordsFromService();
          } else {
            console.log("No news!");
          }
        },
        error: error => console.log("error", error)
      });
    } catch {
      console.log("service unavailable");
      this.serviceAvailable = false;
    }
  }

  private updateRecordListInKnowledgeGraph(data: Record[]) {
    if (!this.knowledgeGraph)
      return;
    // let smallRecords = data.map(function(x) { return extractSmallRecord(x); });
    let smallRecords = data.map(extractSmallRecord);
    this.knowledgeGraph.sendRecordListToGraphTool(smallRecords);
  }

  private async getRecordsFromService() {
    try {
      let data = await firstValueFrom(this.workbenchService.getRecords(this.interventionInfo.id));
      this.recordList = data;
      for (let record of this.recordList) {
        record.storedDisplay = datePipe.transform(record.stored, 'dd. MMMM yyyy');
      }
      this.indicesSortedByDate = this.recordList.map((x, idx) => idx);
      this.indicesSortedByNumberDesc = this.indicesSortedByDate.slice(0);
      this.indicesSortedByNumberDesc.sort((a, b) => Number(this.recordList[b].fileInfo.assignmentNumber)-Number(this.recordList[a].fileInfo.assignmentNumber));

      this.addEntryForNewCluster(this.indicesSortedByDate, 'stored');
      this.addEntryForNewCluster(this.indicesSortedByNumberDesc, 'number');
      this.setSortedRecordIndices();
      this.lastChanged = Date.now();
      this.updateRecordListInKnowledgeGraph(data);
    } catch {
      console.log("service unavailable");
      this.serviceAvailable = false;
    }
  }

  private getField(record: Record, field: string) : string {
    switch (field) {
      case "stored": return record.storedDisplay;
      case "number": return record.fileInfo.assignmentNumber;
    }
    return "";
  }

  private addEntryForNewCluster(indexList: number[], comparedField: string) {
    var n = this.recordList.length - 1;
    while (n > 0) {
      if (this.getField(this.recordList[indexList[n]],comparedField) != this.getField(this.recordList[indexList[n - 1]], comparedField)) {
        indexList.splice(n, 0, -1);
      }
      n--;
    }
    indexList.splice(0, 0, -1);
  }

  private recordTypeToText(type) {
    switch (type) {
      case "singleAnalysis":
        return "Einzelanalyse";
      case "compareToSample":
        return "Mustervergleich";
      case "compareTwoTexts":
        return "Zwei-Text-Vergleich";
      }
  }

  private switchSort() {
    this.isSortedByNumber = !this.isSortedByNumber;
    this.setSortedRecordIndices();
  }

  private switchSortText() : string {
    return this.isSortedByNumber ? 'Datum' : 'Aufgabe';
  }

  private getRecordClusterHeader(record: Record) : string {
    return this.isSortedByNumber ? 'Aufgabe: '+ record.fileInfo.assignmentNumber : record.storedDisplay;
  }

  private getTaskHeader(record: Record) : string {
    return this.isSortedByNumber ? record.storedDisplay : 'Aufgabe '+ record.fileInfo.assignmentNumber;
  }


  private setSortedRecordIndices() {
    this.sortedIndices = this.isSortedByNumber ? this.indicesSortedByNumberDesc : this.indicesSortedByDate;
  }

  private hasRecords() {
    return this.recordList.length > 0;
  }

  private openBlobForIE(newBlob) : boolean {
    // IE doesn't allow using a blob object directly as link href
    // instead it is necessary to use msSaveOrOpenBlob
    let useIE = false;
    if (window.navigator) {
      const nav = (window.navigator as any);
      if (nav.msSaveOrOpenBlob) {
        useIE = true;
        nav.msSaveOrOpenBlob(newBlob);
      }
    }
    return useIE;
  }

  private downloadTextFile(statementID, textType, filename): void {
    this.workbenchService.getTextFile(this.interventionInfo.id, statementID, textType)
      .subscribe(x => {
        // It is necessary to create a new blob object with mime-type explicitly set
        // otherwise only Chrome works like it should
        var newBlob = new Blob([x], { type: "text/plain" });

        if (this.openBlobForIE(newBlob)) {
          return;
        }

        // For other browsers:
        // Create a link pointing to the ObjectURL containing the blob.
        const data = window.URL.createObjectURL(newBlob);

        var link = document.createElement('a');
        link.href = data;
        //always create txt, even if original file was pdf
        link.download = filename.substring(0, filename.length - 3) + "txt";
        // this is necessary as link.click() does not work on the latest firefox
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

        setTimeout(function () {
          // For Firefox it is necessary to delay revoking the ObjectURL
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      });
  }

  private deleteLRSitem(itemId) {
    if (confirm(this.textAnalysisInfo.eraseWarning)) {
      this.workbenchService.deleteLRSitem(this.interventionInfo.id, itemId).subscribe((data: string) => {
        this.getRecordsFromService();
      })
    }
  }

}
