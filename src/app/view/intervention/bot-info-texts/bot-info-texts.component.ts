import { Component, Input, OnInit } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { BotInfo } from 'src/app/model/botInfo';
import { Intervention } from 'src/app/model/intervention';
import { WorkbenchService } from 'src/app/services/workbench.service';

@Component({
  selector: 'intervention-bot-info-texts',
  templateUrl: './bot-info-texts.component.html',
  styleUrls: ['./bot-info-texts.component.css']
})
export class BotInfoTextsComponent implements OnInit {
  @Input() interventionInfo: Intervention;
  @Input() botInfo: BotInfo;

  isBotOnline: boolean = true;

  constructor(
    private workbenchService: WorkbenchService
  ) { }

  ngOnInit(): void {
    this.checkBotIsOnline();
  }

  private async checkBotIsOnline() {
    var botName = this.botInfo.nameFramework;
    if (botName) {
      try {
        var response = await firstValueFrom(this.workbenchService.isBotOnline(botName));
        this.isBotOnline = response == 'yes';
        console.log('Bot', botName, 'is online:', this.isBotOnline);
      } catch {
        console.log('Online status for Bot', botName, 'is not available (service not running?)');
        this.isBotOnline = false;
      }
    }
  }

}
