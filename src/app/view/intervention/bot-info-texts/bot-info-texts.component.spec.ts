import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BotInfoTextsComponent } from './bot-info-texts.component';

describe('BotInfoTextsComponent', () => {
  let component: BotInfoTextsComponent;
  let fixture: ComponentFixture<BotInfoTextsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BotInfoTextsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BotInfoTextsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
