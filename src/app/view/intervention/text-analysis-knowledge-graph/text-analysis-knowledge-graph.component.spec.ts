import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextAnalysisKnowledgeGraphComponent } from './text-analysis-knowledge-graph.component';

describe('TextAnalysisKnowledgeGraphComponent', () => {
  let component: TextAnalysisKnowledgeGraphComponent;
  let fixture: ComponentFixture<TextAnalysisKnowledgeGraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextAnalysisKnowledgeGraphComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextAnalysisKnowledgeGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
