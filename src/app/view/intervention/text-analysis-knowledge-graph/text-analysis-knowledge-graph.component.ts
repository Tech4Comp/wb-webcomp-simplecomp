import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { MwbConfiguration } from 'src/app/configuration/mwb.config';
import { BotInfo } from 'src/app/model/botInfo';
import { Intervention } from 'src/app/model/intervention';

@Component({
  selector: 'intervention-text-analysis-knowledge-graph',
  templateUrl: './text-analysis-knowledge-graph.component.html',
  styleUrls: ['./text-analysis-knowledge-graph.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TextAnalysisKnowledgeGraphComponent implements OnInit {
  @Input() interventionInfo: Intervention;

  botInfo: BotInfo;
  botNameRocketChat: string;

  constructor(   
    private mwb: MwbConfiguration,
  ) { }

  ngOnInit(): void {
    this.botInfo = this.mwb.configuration.getBotInfo(this.interventionInfo.id)
    this.botNameRocketChat = this.mwb.configuration.getBotNameForRocketChat(this.interventionInfo.id);
 }

}
