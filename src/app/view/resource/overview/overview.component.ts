import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MwbConfiguration } from 'src/app/configuration/mwb.config';
import { Resource } from 'src/app/model/resource';
import { UserInfo } from 'src/app/model/userInfo';
import { environment } from 'src/environments/environment';
import { HelpComponent } from '../../utils/help/help.component';

@Component({
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {
  @ViewChild('help') help: HelpComponent ; 
  hideOverview: boolean = false;
  resourcesToMaintain : Resource[];
  resourcesUsed : Resource[];
  mayCreateNewResource: boolean = false;
  private title = 'Mentoring Workbench - Übersicht';

  constructor(
    private userInfo: UserInfo,
    private mwb: MwbConfiguration,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.hideOverview = true;
    this.userInfo.setTitle(this.title);
    this.resourcesToMaintain = [];
    this.resourcesUsed = [];
    this.mayCreateNewResource = false;
    console.log('Environment production:', environment.production);
    // this.hideOverview = environment.production;
    if (!this.hideOverview) {
      this.resourcesToMaintain = this.hideOverview ? [] : 
        this.mwb.configuration.getMaintainedResources()
          .map(x => this.mwb.configuration.getResourceInfo(x))
          .sort((x, y) => y.lastChanged - x.lastChanged)
      this.resourcesUsed = this.hideOverview ? [] : 
        this.mwb.configuration.getUsedResources()
          .map(x => this.mwb.configuration.getResourceInfo(x))
          .sort((x, y) => y.lastChanged - x.lastChanged)
      this.mayCreateNewResource = this.hideOverview || this.mwb.configuration.mayCreateNewResource();
    }
  }


  public createResource() {
    let createText = 'Erstelle neue Mentoring Workbench';
    this.help.confirmCreate(createText).then(() => {
      let newId = this.mwb.configuration.createResource();
      this.router.navigate(['/mentor/resource/', newId, 'settings']);
    })    
    .finally(() => {});
  }

}
