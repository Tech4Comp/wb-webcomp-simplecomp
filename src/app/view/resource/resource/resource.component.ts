import { Component, OnInit } from '@angular/core';
import { Intervention, InterventionType, IsInterventionWithTextAnalysis } from "src/app/model/intervention";
import { Resource } from "src/app/model/resource";
import { DatePipe } from '@angular/common';
import { UserInfo } from 'src/app/model/userInfo';
import { WorkbenchService } from 'src/app/services/workbench.service';
import { MwbConfiguration } from 'src/app/configuration/mwb.config';
import { Observable } from 'rxjs';

const datePipe = new DatePipe('de');

@Component({
  templateUrl: './resource.component.html',
  styleUrls: [
    './resource.component.css'
  ]
})
export class ResourceComponent implements OnInit {
  resourceId: string;
  resourceInfo: Resource;
  interventions = [];
  isValidResource: boolean = false;
  fulfilled: number[] = [];
  nrOfTasks: number[] = [];
  enumInterventionType;

  constructor(
    private mwb: MwbConfiguration,
    private mwbService: WorkbenchService,
    private userInfo: UserInfo
  ) { }

  ngOnInit(): void {
    this.resourceId = this.userInfo.resourceId;
    this.isValidResource = this.mwb.configuration.isValidResource(this.resourceId);
    this.resourceInfo = this.mwb.configuration.getResourceInfo(this.resourceId);
    this.interventions = this.mwb.configuration.getInterventionsFor(this.resourceId);
    this.enumInterventionType = InterventionType;
    this.getFulfilledTasksInfo();
  }

  public getFulfilledTasksInfo() {
    this.interventions.forEach((intervention, idx) => {
      this.fulfilled.push(-1);
      this.nrOfTasks.push(0);
      if (IsInterventionWithTextAnalysis(intervention.type)) {
        this.nrOfTasks[idx] = this.mwb.configuration.getNumberOfWritingTasks(intervention.id);
        this.mwbService.getCntFulfilledTasks(intervention.id).subscribe((data: number) => {
          this.fulfilled[idx] = data;
        });
      }
    });
  }

}
