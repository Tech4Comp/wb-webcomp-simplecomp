import { Component, Input, OnInit } from '@angular/core';
import { Resource } from 'src/app/model/resource';
import { UserInfo } from 'src/app/model/userInfo';
import { forceName } from '../../utils/utils';

@Component({
  selector: 'app-resource-info',
  templateUrl: './resource-info.component.html',
  styleUrls: ['./resource-info.component.css']
})
export class ResourceInfoComponent implements OnInit {
  @Input() resourceInfo: Resource;
  @Input() isValidResource: Boolean;

  constructor(
    private userInfo: UserInfo
  ) { }

  ngOnInit(): void {
    this.setTitle();
  }

  private setTitle() {
    this.userInfo.setTitle(this.getResourceName())
  }

  private getResourceName() {
    return forceName(this.resourceInfo.name);
  }
}
