import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, switchMap, tap } from 'rxjs/operators';
import { UserInfo } from '../model/userInfo';
import { LoginService } from "../services/login-service";
import { PrivacyService } from '../services/privacy-service';

@Injectable({ providedIn: 'root' })
export class LoginGuard implements CanActivate {
  public static readonly loginUrl = '/login';

  constructor(
    private loginService: LoginService,
    private userInfo: UserInfo,
    private router: Router
  ) {
  }

  public canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    this.setResourceAndInterventionId(route);
    return this.loginService.isDoneLoading$
      .pipe(
        filter(isDone => isDone),
        switchMap(_ => this.loginService.isAuthenticated$),
        tap(isAuthenticated => isAuthenticated || this.router.navigate([LoginGuard.loginUrl], { queryParams: { redirect: state.url }}))
      );
  }

  private setResourceAndInterventionId(route: ActivatedRouteSnapshot) {
    let currentBasePath = '/' + route.url[0].path;
    if (currentBasePath !== "/logout")
    {
      this.userInfo.setResourceId(route.params['resId']);
      this.userInfo.setInterventionId(route.params['intId']);
    }
    this.loginService.setupAuthentication(this.userInfo.resourceId, this.userInfo.interventionId);
  }
}


@Injectable({ providedIn: 'root' })
export class PrivacyGuard implements CanActivate {
  public static readonly privacyUrl = 'privacy';

  constructor(
    private userInfo: UserInfo,
    private privacyService: PrivacyService,
    private router: Router
  ) {
  }

  public canActivate(): boolean {
    let hasConfirmedPrivacyStatement = !this.privacyService.needsPrivacyStatement() || this.privacyService.hasAgreedPrivacyStatement();
    if (hasConfirmedPrivacyStatement)
      return true;
      this.router.navigate([PrivacyGuard.privacyUrl,'resource', this.userInfo.resourceId]);
    }
}


@Injectable({ providedIn: 'root' })
export class LoginAndPrivacyGuard implements CanActivate {

  constructor(
    private userInfo: UserInfo,
    private loginGuard: LoginGuard,
    private privacyService: PrivacyService,
    private router: Router
  ) {
  }

  public canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> {
    return this.loginGuard.canActivate(route,state)
      .pipe(
        tap(_ => this.hasConfirmedPrivacyStatement() || this.router.navigate([PrivacyGuard.privacyUrl,'resource', this.userInfo.resourceId]))
      );
  }

  private hasConfirmedPrivacyStatement() {
    return !this.privacyService.needsPrivacyStatement() || this.privacyService.hasAgreedPrivacyStatement();
  }
}

export function redirectToResourceAndIntervention(router: Router, resourceId: string, interventionId: string) {
  let redirectUri = getResourceAndInterventionUrl(resourceId, interventionId);
  router.navigateByUrl(redirectUri);
}

export function getResourceAndInterventionUrl(resourceId: string, interventionId: string) : string {
  if (interventionId)
    return '/intervention/' + interventionId;
  if (resourceId)
    return '/resource/' + resourceId;
  return  '';
}

export function redirectToUrlInQueryParams(route: ActivatedRoute, router: Router) {
  let redirectUri = extractUrlFromQueryParams(route);
  if (redirectUri)
    router.navigateByUrl(redirectUri);
}

export function extractUrlFromQueryParams(route: ActivatedRoute) : string {
  let redirectUri = '/';
  route.queryParams.subscribe(params => {
  if (params['redirect'])
    redirectUri = params['redirect'];
  });
  return redirectUri;
}




