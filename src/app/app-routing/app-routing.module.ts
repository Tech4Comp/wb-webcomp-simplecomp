import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from 'src/app/view/basic/login/login.component';
import { PrivacyComponent } from 'src/app/view/basic/privacy/privacy.component';
import { OverviewComponent } from 'src/app/view/resource/overview/overview.component';
import { InterventionComponent } from 'src/app/view/intervention/intervention.component';
import { ResourceComponent } from 'src/app/view/resource/resource/resource.component';
import { LoginAndPrivacyGuard, LoginGuard, PrivacyGuard } from 'src/app/app-routing/guard';
import { WelcomeComponent } from 'src/app/view/basic/welcome/welcome.component';
import { LrsAnalyticsComponent } from 'src/app/view/lrs-analytics/lrs-analytics.component';
import { BotAnalysisComponent } from 'src/app/view/bot-analysis/bot-analysis.component';
import { LogoutComponent } from 'src/app/view/basic/logout/logout.component';
import { ResourceEditComponent } from 'src/app/view/mentor/resource-edit/resource-edit.component';
import { InterventionEditComponent } from '../view/mentor/intervention-edit/intervention-edit.component';
import { ResourceEditInterventionsComponent } from '../view/mentor/resource-edit-interventions/resource-edit-interventions.component';
import { ResourceMentorResultsComponent } from '../view/mentor/resource-mentor-results/resource-mentor-results.component';
import { ExamFeedbackEditComponent as ExamFeedbackEditComponent } from '../view/mentor/exam-feedback-edit/exam-feedback-edit.component';
import { WritingTaskEditComponent } from '../view/mentor/writing-task-edit/writing-task-edit.component';

const routes: Routes = [
  { path: 'welcome', component: WelcomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', canActivate: [LoginGuard], component: LogoutComponent },
  { path: 'overview', canActivate: [LoginGuard], component: OverviewComponent },
  { path: 'analytics', canActivate: [LoginGuard], component: LrsAnalyticsComponent },
  { path: 'botAnalysis', canActivate: [LoginGuard], component: BotAnalysisComponent },
  { path: PrivacyGuard.privacyUrl+'/resource/:resId', canActivate: [LoginGuard], component: PrivacyComponent },

  { path: 'mentor/resource/:resId/settings', canActivate: [LoginGuard], component: ResourceEditComponent },
  { path: 'mentor/resource/:resId/interventions', canActivate: [LoginGuard], component: ResourceEditInterventionsComponent },
  { path: 'mentor/resource/:resId/results', canActivate: [LoginGuard], component: ResourceMentorResultsComponent },
  { path: 'mentor/intervention/:intId', canActivate: [LoginGuard], component: InterventionEditComponent },
  { path: 'mentor/ExamFeedbackItem/:itemId', canActivate: [LoginGuard], component: ExamFeedbackEditComponent },
  { path: 'mentor/TextAnalysisItem/:itemId', canActivate: [LoginGuard], component: WritingTaskEditComponent },

  { path: 'resource/:resId', canActivate: [LoginAndPrivacyGuard], component: ResourceComponent },
  { path: 'resource/:resId/:sth', redirectTo: '/resource/:resId', pathMatch: 'full' },
  { path: 'resource/:sth', redirectTo: '/overview', pathMatch: 'full' },

  { path: 'intervention/:intId', canActivate: [LoginAndPrivacyGuard], component: InterventionComponent },
  { path: 'intervention/:intId/:sth', redirectTo: 'intervention/:intId', pathMatch: 'full' },
  
  { path: '**',   redirectTo: '/welcome', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
