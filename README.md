# Simplecomp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Production Build

Run `npm run build:prod`to build the project for the production environment. The build artifacts will be stored in the `elements/` directory.

## Generate IMS-CP

Run `npm run build:imscp` to generate the imscp archive. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Creating a new release

- Have finished version in develop branch
- change version in package.json
- `git flow release start v...`
- `git flow release finish v...`
- `git push origin --tags`
- `git push`
- `git checkout master`
- `git push`

(The Docker Container is automatically built. When it's finished, you can see it in the Kubernetes Dashboard: https://tech4comp.dbis.rwth-aachen.de)

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
