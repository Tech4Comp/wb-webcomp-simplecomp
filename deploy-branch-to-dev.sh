#!/bin/sh
CA_CERT=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt
TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
curl -k -X PUT --cacert $CA_CERT -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" -d @workbench-branch-to-dev.json "https://137.226.232.175:6443/apis/apps/v1/namespaces/mztud-test/deployments/workbench-dev"
POD_NAME=$(curl -k --cacert $CA_CERT -H "Authorization: Bearer $TOKEN" "https://137.226.232.175:6443/api/v1/namespaces/mztud-test/pods" | jq '.items[] | .metadata.name' | grep -v 't4c-workbench-dev' | grep -m 1 'workbench-dev' | sed 's/"//g')
curl -k -X DELETE --cacert $CA_CERT -H "Authorization: Bearer $TOKEN" "https://137.226.232.175:6443/api/v1/namespaces/mztud-test/pods/$POD_NAME"
