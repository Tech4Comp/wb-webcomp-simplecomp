import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getLoginButtonText() {
    return element(by.css('.btn-login')).getText() as Promise<string>;
  }
}
