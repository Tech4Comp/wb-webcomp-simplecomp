const fs = require('fs-extra');
const concat = require('concat');

(async function build() {

    const files = [
        './src/js/setattributes.js',
        './dist/simplecomp/runtime.js',
        './dist/simplecomp/polyfills.js',
        './dist/simplecomp/scripts.js',
        './dist/simplecomp/main.js'
    ];

    await fs.ensureDir('elements');
    await concat(files, 'elements/simplecomp.js');

    fs.readFile('elements/simplecomp.js', 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      fs.writeFile('elements/simplecomp.js', data, 'utf8', function (err) {
         if (err) return console.log(err);
      });
    });

    await fs.copy('./elements/simplecomp.js', './ims-cp/simplecomp.js');

    await fs.copy('./dist/simplecomp/styles.css', './elements/styles.css');
    await fs.copy('./dist/simplecomp/styles.css', './ims-cp/styles.css');

    await fs.copy('./ims-cp/index.html', 'elements/index.html');

    // copy to dist
    await fs.remove('./dist/simplecomp/scripts.js');
    await fs.remove('./dist/simplecomp/runtime.js');
    await fs.remove('./dist/simplecomp/polyfills.js');
    await fs.remove('./dist/simplecomp/main.js');
    await fs.remove('./dist/simplecomp/runtime-es2015.js');
    await fs.remove('./dist/simplecomp/polyfills-es2015.js');
    await fs.remove('./dist/simplecomp/main-es2015.js');
    await fs.remove('./dist/simplecomp/runtime-es5.js');
    await fs.remove('./dist/simplecomp/polyfills-es5.js');
    await fs.remove('./dist/simplecomp/main-es5.js');
    await fs.copy('./ims-cp/index.html', './dist/simplecomp/index.html');
    await fs.copy('./elements/simplecomp.js', './dist/simplecomp/simplecomp.js');

})();
